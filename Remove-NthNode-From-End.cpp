// TC: O(n)  Space: O(1)
// Link: https://leetcode.com/problems/remove-nth-node-from-end-of-list/
class Solution {
   public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        ListNode *cur = head;
        int nodeCnt = 0;
        ListNode *tmp;
        while (cur) {
            cur = cur->next;
            nodeCnt++;
        }
        if (nodeCnt == n) {
            tmp = head;
            head = head->next;
            delete tmp;
            return head;
        }
        cur = head;
        int jmp = nodeCnt - n - 1;
        while (jmp > 0) {
            cur = cur->next;
            jmp--;
        }

        if (cur && cur->next) {
            ListNode *tmp = cur->next;
            cur->next = cur->next->next;
            delete tmp;
        }

        return head;
    }
};