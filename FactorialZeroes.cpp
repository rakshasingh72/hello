// TC: log5(n)  Space: O(1)
// Link: https://leetcode.com/problems/factorial-trailing-zeroes/

class Solution {
   public:
    int trailingZeroes(int n) {
        int ret = 0;
        while (n > 0) {
            ret += n / 5;
            n = n / 5;
        }
        return ret;
    }
};