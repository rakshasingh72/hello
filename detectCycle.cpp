#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

typedef struct Graph {
    int numVertices;  
    struct Node **A;

    Graph(int v) {
        A = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest);
        newNode->next = A[src];
        newNode->weight = w;
        A[src] = newNode;
    }
    struct Node *createNode(int v) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = v;
        newNode->next = NULL;
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = A[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
    bool IsDag(int v) {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));
        vector<int> recStack;
        for (int i = 0; i < numVertices; i++) {
            if (visited[v] == 0) {
                if (dfsHelper(v, visited, recStack)) {
                    return true;
                }
            }
            v = (v + 1) % numVertices;
        }
        return false;
    }
    bool dfsHelper(int v, int *visited, vector<int> &recStack) {
        visited[v] = 1;
        recStack.push_back(v);
        struct Node *neighbor = A[v];
        while (neighbor != NULL) {
            int vv = neighbor->vertex;
            vector<int>::iterator it =find(recStack.begin(), recStack.end(), vv);
            vector<int>::iterator itend = recStack.end();
            if (it != itend) {
                return true;
            }
            if (visited[neighbor->vertex] == 0) {
                if (dfsHelper(neighbor->vertex, visited, recStack)) {
                    return true;
                };
            }
            neighbor = neighbor->next;
        }
        recStack.pop_back();
        return false;
    }
} graph;

int main() {
    int v = 5;

    // vector<vector<int>> edges = {{0, 1, 1}, {0, 3, 1}, {1, 2, 1}, {1, 3, 1},
    //                              {1,4, 1}, {2, 4, 1}, {3, 4, 1}};
    vector<vector<int>> edges = {{0, 1, 1}, {0, 3, 1}, {1, 2, 1}, {1, 3, 1},
                                 {4, 1, 1}, {2, 4, 1}, {3, 4, 1}};

    graph g(v);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }
    g.printGraph();
    cout << "Is DAG: " << g.IsDag(0) << endl;
    return 0;
}
//  Adjacency list
// [0]: 3, 1, 
// [1]: 4, 3, 2, 
// [2]: 4, 
// [3]: 4, 
// [4]: 
// Is DAG: 0
//  Adjacency list
// [0]: 3, 1, 
// [1]: 3, 2, 
// [2]: 4, 
// [3]: 4, 
// [4]: 1, 
// Is DAG: 1
