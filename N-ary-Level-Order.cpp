// TC: O(n)  Space:O(height) recursion
// Link: https://leetcode.com/problems/n-ary-tree-level-order-traversal/

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
   public:
    void dfs(Node* root, int level, vector<vector<int>>& ans) {
        if (root == NULL) return;
        if (level == ans.size()) ans.push_back(vector<int>{});
        ans[level].push_back(root->val);
        for (int i = 0; i < root->children.size(); i++) {
            dfs(root->children[i], level + 1, ans);
        }
    }
    vector<vector<int>> levelOrder(Node* root) {
        vector<vector<int>> ans;
        dfs(root, 0, ans);
        return ans;
    }
};