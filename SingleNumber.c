// TC: O(n)  Space:O(1)
// Link: https://leetcode.com/problems/single-number/

int singleNumber(int* nums, int numsSize) {
    int single = nums[0];

    for (int i = 1; i < numsSize; i++) {
        single = single ^ nums[i];
    }

    return single;
}
