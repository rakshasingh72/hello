#include <bits/stdc++.h>

using namespace std;

struct Vertex {
    int name;
    int key;
    int parent;
    bool operator==(const Vertex& n1) { return n1.name == name; }
};

int** get2DMatrix(int r, int c) {
    int** A = (int**)malloc(r * (sizeof(int*)));
    for (int i = 0; i < r; i++) {
        A[i] = (int*)malloc(c * sizeof(int));
    }
    return A;
}

class comparator {
   public:
    int operator()(const Vertex& x, const Vertex& y) { return x.key > y.key; }
};

typedef struct Graph {
    int** Adj;
    int numVertices;
    Graph(int n) {
        numVertices = n;
        Adj = get2DMatrix(numVertices, numVertices);
    }
    void addEdge(int src, int dest, int weight) {
        Adj[src][dest] = weight;
        Adj[dest][src] = weight;
    }
    void printGraph() {
        printf("Matrix\n");
        for (int r = 0; r < numVertices; r++) {
            for (int c = 0; c < numVertices; c++) {
                cout << Adj[r][c] << "  ";
            }
            cout << endl;
        }
        cout << endl;
    }

    vector<Vertex> getMstByPrims() {
        vector<Vertex> minHeap;
        vector<Vertex> vertexSet(numVertices);

        int r = 0;
        vertexSet[r].name = 0;
        vertexSet[r].key = 0;
        vertexSet[r].parent = -1;
        minHeap.push_back(vertexSet[r]);

        for (int i = 1; i < numVertices; i++) {
            vertexSet[i].name = i;
            vertexSet[i].key = 9999;
            vertexSet[i].parent = -1;
            minHeap.push_back(vertexSet[i]);
        }

        make_heap(minHeap.begin(), minHeap.end(), comparator());

        while (!minHeap.empty()) {
            // Extract min
            int u = minHeap.front().name;
            pop_heap(minHeap.begin(), minHeap.end(), comparator());
            minHeap.pop_back();

            for (int v = 0; v < numVertices; v++) {
                if (Adj[u][v] > 0) {
                    Vertex neighbor;
                    neighbor.name = v;
                    auto itr = find(minHeap.begin(), minHeap.end(), neighbor);
                    // v in HEAP
                    if (itr != minHeap.end() && Adj[u][v] < itr->key) {
                        vertexSet[v].parent = u;
                        vertexSet[v].key = Adj[u][v];
                        itr->parent = u;
                        itr->key = Adj[u][v];
                    }
                }
            }
            make_heap(minHeap.begin(), minHeap.end(), comparator());
        }
        return vertexSet;
    }
} graph;
int main() {
    graph g(4);
    vector<vector<int>> e = {
        {0, 1, 10}, {0, 2, 30}, {0, 3, 40}, {1, 2, 20}, {2, 3, 50}};

    for (int i = 0; i < e.size(); i++) {
        g.addEdge(e[i][0], e[i][1], e[i][2]);
    }
    vector<Vertex> res = g.getMstByPrims();
    for (auto x : res) {
        cout << x.key << "  ";
    }
    return 0;
}
