// TC: O(1)  Space: O(1)
// Link: https://leetcode.com/problems/power-of-three/

bool isPowerOfThree(int n) {
    if (n < 1) return false;

    if (fmod(log10(n) / log10(3), 1) == 0) return true;

    return false;
}
