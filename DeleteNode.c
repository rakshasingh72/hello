// TC: O(1) Space: O(1)
// Link: https://leetcode.com/problems/delete-node-in-a-linked-list/

void deleteNode(struct ListNode* node) {
    node->val = node->next->val;

    struct node* temp = node->next;

    node->next = node->next->next;

    free(temp);
}