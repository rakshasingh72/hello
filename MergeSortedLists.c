// TC:O(n)  n=Length of smaller list  Space:O(1)
// Link: https://leetcode.com/problems/merge-two-sorted-lists/

struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2) {
    struct ListNode* h = NULL;
    struct ListNode* prev = NULL;
    struct ListNode* temp = NULL;
    int i = 0;

    if (l1 == NULL && l2 == NULL)
        return NULL;
    else if (l1 == NULL)
        return l2;
    else if (l2 == NULL)
        return l1;

    while (l1 != NULL && l2 != NULL) {
        if (l1->val <= l2->val) {
            if (i == 0) {
                h = l1;
                i++;
            }
            prev = l1;
            temp = l1->next;
            l1->next = l2;
            l1 = temp;
        } else {
            if (i == 0) {
                h = l2;
                i++;
            }
            prev = l2;
            temp = l2->next;
            l2->next = l1;
            l2 = temp;
        }
    }
    if (prev) {
        if (l1 == NULL)
            prev->next = l2;
        else
            prev->next = l1;
    }
    return h;
}