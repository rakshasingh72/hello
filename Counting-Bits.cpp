// TC: O(n*sizeof(int)) Space: O(1)
// Link: https://leetcode.com/problems/counting-bits/

class Solution {
   public:
    vector<int> countBits(int num) {
        vector<int> ret;
        for (int i = 0; i <= num; i++) {
            int count = 0;
            int n = i;
            while (n != 0) {
                n = n & (n - 1);
                count++;
            }
            ret.push_back(count);
        }

        return ret;
    }
};