// TC: O(n^2)  Space: O(height) recursion
// Link: https://leetcode.com/problems/path-sum-iii/

class Solution {
   public:
    int pathSum(TreeNode* root, int sum) {
        int count = 0, pSum = 0;
        if (root) {
            count = pathCount(root, pSum, sum) + pathSum(root->left, sum) +
                    pathSum(root->right, sum);
            return count;
        }
        return 0;
    }
    int pathCount(TreeNode* root, int sumSoFar, int sum) {
        int cnt = 0;
        if (root) {
            sumSoFar += root->val;
            if (sumSoFar == sum) cnt++;
            int leftCnt = pathCount(root->left, sumSoFar, sum);
            int rightCnt = pathCount(root->right, sumSoFar, sum);
            return cnt + leftCnt + rightCnt;
        }
        return 0;
    }
};