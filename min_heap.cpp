#include <bits/stdc++.h>
#include <iostream>
using namespace std;
// left=2i+1     right=2i+2

void swap(int &x, int &y) {
    int t;
    t = x;
    x = y;
    y = t;
}
class MinHeap {
   public:
    int *A;
    int capacity;
    int size;  // heap size
    MinHeap(int cap) {
        capacity = cap;
        size = 0;
        A = (int *)malloc(cap * sizeof(int));
    }
    int insert(int key) {
        if (size == capacity) {
            cout << "Min-Heap is full " << endl;
            return -1;
        }
        int last_idx = size;
        A[last_idx] = key;
        size++;
        swim(last_idx);
    }
    void deleteKey(int i) {
        if (i < size - 1) {
            swap(A[i], A[size - 1]);
            size--;
            sink(i);
        }
    }
    void updateKey(int i, int val) {
        if (val == A[i]) {
            return;
        } else if (val < A[i]) {
            A[i] = val;
            swim(i);
        } else {
            A[i] = val;
            sink(i);
        }
    }
    int extractMin() {
        if (size <= 0) {
            return -1;
        }
        swap(A[0], A[size - 1]);
        size--;
        sink(0);
    }
    int getMin() {
        // if empty return error
        if (size <= 0) {
            return -1;
        }
        return A[0];
    }

    void printHeap() {
        cout << "Current Min-Heap is:" << endl;
        for (int i = 0; i < size; i++) {
            cout << A[i] << "  ";
        }
        cout << endl;
    }

   private:
    void swim(int i) {
        while (1) {
            int p = (i - 1) / 2;
            if (i <= 0 || A[p] < A[i]) {
                break;
            } else {
                swap(A[p], A[i]);
                i = p;
            }
        }
    }
    void sink(int i) {
        int min, l, r;
        while (1) {
            min = i;
            l = 2 * i + 1;
            r = 2 * i + 2;
            if (l < size - 1 && A[l] < A[min]) {
                min = l;
            }
            if (r < size - 1 && A[r] < A[min]) {
                min = r;
            }
            if (min == i) {
                break;
            }
            swap(A[min], A[i]);
            i = min;
        }
    }
};

int main() {
    MinHeap m(10);
    m.insert(24);
    m.insert(23);
    m.insert(22);
    m.insert(21);
    m.insert(20);
    m.insert(19);
    m.insert(18);
    m.printHeap();  // Current Min-Heap is: 18  21  19  24  22  23  20

    m.updateKey(3, 5);  // Current Min-Heap is: 5  18  19  21  22  23  20
    m.printHeap();
    m.deleteKey(0);  //  Current Min-Heap is: 18  20  19  21  22  23

    m.printHeap();  // Current Min-Heap is: 18 20 19 21 22 23
    m.extractMin();
    m.printHeap();  // Current Min-Heap is: 19  20  23  21  22
    return 0;
}