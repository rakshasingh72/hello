// TC: O(n)  Space: O(1)
// Link: https://leetcode.com/problems/swap-nodes-in-pairs/

class Solution {
   public:
    ListNode *swapPairs(ListNode *head) {
        if (head == NULL || head->next == NULL) return head;
        ListNode *cur = head;

        head = head->next;
        while (cur && cur->next) {
            ListNode *tmp = cur->next;
            cur->next = cur->next->next;
            tmp->next = cur;
            ListNode *prev = cur;
            cur = cur->next;
            if (cur && cur->next) prev->next = cur->next;
        }

        return head;
    }
};
