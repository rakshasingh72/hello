// TC:O(hn)  h=length of haystack, n=length of needle
// Link:https://leetcode.com/problems/implement-strstr/

class Solution {
   public:
    int strStr(string h, string n) {
        int hsize = h.size();
        int nsize = n.size();
        if (hsize == 0 && nsize == 0) {
            return 0;
        }
        int j, start, end;
        for (int i = 0; i < hsize; i++) {
            start = i;
            j = 0;
            while (h[i] == n[j] || n[j] == '\0') {
                if (n[j] == '\0') {
                    return start;
                }
                i++;
                j++;
            }

            i = start;
        }
        return -1;
    }
};