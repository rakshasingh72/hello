// TC: O(n)  Space: O(1)
// Link: https://leetcode.com/problems/fizz-buzz/
/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
char** fizzBuzz(int n, int* returnSize) {
    *returnSize = n;
    char** charArr = (char**)malloc(n * sizeof(char*));
    for (int i = 1; i <= n; i++) {
        if (i % 3 == 0 && i % 5 == 0) {
            charArr[i - 1] = malloc(strlen("FizzBuzz") + 1);
            strcpy(charArr[i - 1], "FizzBuzz");
        } else if (i % 3 == 0) {
            charArr[i - 1] = malloc(strlen("Fizz") + 1);
            strcpy(charArr[i - 1], "Fizz");
        }
        else if (i % 5 == 0) {
            charArr[i - 1] = malloc(strlen("Buzz") + 1);
            strcpy(charArr[i - 1], "Buzz");
        }
        else {
            char buf[11];
            sprintf(buf, "%d", i);
            charArr[i - 1] = malloc(strlen(buf) + 1);
            strcpy(charArr[i - 1], buf);
        }
    }
    return charArr;
}
