// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/n-ary-tree-preorder-traversal/
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
    vector<int> ans;

   public:
    vector<int> preorder(Node* root) {
        if (root == NULL) return ans;
        ans.push_back(root->val);
        for (int i = 0; i < root->children.size(); i++) {
            preorder(root->children[i]);
        }
        return ans;
    }
};