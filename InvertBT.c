// TC: O(n)  Space:O(height) recursion
// Link: https://leetcode.com/problems/invert-binary-tree/

void childSwap(struct TreeNode* node) {
    struct TreeNode* tmp;
    tmp = node->left;
    node->left = node->right;
    node->right = tmp;
}
struct TreeNode* invertTree(struct TreeNode* root) {
    if (!root) return NULL;
    childSwap(root);
    invertTree(root->left);
    invertTree(root->right);
    return root;
}