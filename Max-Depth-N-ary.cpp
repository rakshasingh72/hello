// TC: O(n) n=Total nodes  Space: O(height) recursion
// Link: https://leetcode.com/problems/maximum-depth-of-n-ary-tree/

/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/
class Solution {
   public:
    int maxDepth(Node* root) {
        if (root == NULL) return 0;
        int curMax = 0;
        int numChildren = root->children.size();
        for (int i = 0; i < numChildren; i++) {
            curMax = max(curMax, maxDepth(root->children[i]));
        }
        return 1 + curMax;
    }
};