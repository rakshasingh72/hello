// TC:O(mn) m=length of min length string,n=no. of strings   Space:O(m)
// Link: https://leetcode.com/problems/longest-common-prefix/

class Solution {
   public:
    string longestCommonPrefix(vector<string>& s) {
        int n = s.size();
        string ret = "";
        if (n == 0) {
            return ret;
        }
        int firstStrLen = s[0].size();
        int i = 0, j = 0;
        char tmp;

        for (int j = 0; j < firstStrLen; j++) {
            tmp = s[i][j];
            for (int i = 0; i < n; i++) {
                if (s[i].size() <= j || s[i][j] != tmp) {
                    return ret;
                }
            }
            ret += tmp;
        }
        return ret;
    }
};