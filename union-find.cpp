#include <bits/stdc++.h>
using namespace std;

class UnionFind {
    vector<int> parent;
    vector<int> size;

   public:
    UnionFind(int n) {
        parent.resize(n);
        size.resize(n);
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
    }
    int root(int i) {
        while (i != parent[i]) {
            i = parent[i];
            parent[i] = parent[parent[i]];  // path compression
        }
        return i;
    }
    bool isConnected(int p, int q) { return root(p) == root(q); }

    void Union(int p, int q) {
        int i = root(p);
        int j = root(q);
        if (size[i] < size[j]) {  // union by size
            parent[i] = j;
            size[j] += size[i];
        } else {
            parent[j] = i;
            size[i] += size[j];
        }
    }
};

int main() {
    UnionFind uf(10);

    uf.Union(0, 1);  // C1: 0-1-2-3 , C2: 4-5-6 , C3:7-8 , C4: 9
    uf.Union(0, 2);
    uf.Union(1, 2);
    uf.Union(1, 3);
    uf.Union(4, 5);
    uf.Union(4, 6);
    uf.Union(7, 8);

    bool c = uf.isConnected(1, 2);

    cout << "isConnected=" << endl;
    cout << uf.isConnected(1, 3) << endl;
    cout << uf.isConnected(3, 4) << endl;
    cout << uf.isConnected(4, 6) << endl;
    cout << uf.isConnected(7, 8) << endl;
    cout << uf.isConnected(0, 9) << endl;

    return 0;
}
// isConnected=
// 1
// 0
// 1
// 1
// 0