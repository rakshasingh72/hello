// TC: O(n^2)  Space: O(height) recursion
// Link: https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/
class Solution {
   public:
    TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
        if (root == NULL || p == root || q == root) return root;

        pair<bool, bool> ret{false, false};
        ret = search(root->left, p, q, ret);
        if (ret == make_pair(false, true) || ret == make_pair(true, false))
            return root;
        else if (ret == make_pair(true, true))
            return lowestCommonAncestor(root->left, p, q);

        return lowestCommonAncestor(root->right, p, q);
    }
    pair<bool, bool> search(TreeNode* root, TreeNode* p, TreeNode* q,
                            pair<bool, bool>& ret) {
        if (p == root) ret.first = true;
        if (q == root) ret.second = true;
        if (root) {
            search(root->left, p, q, ret);
            search(root->right, p, q, ret);
        }
        return ret;
    }
};