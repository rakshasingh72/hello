/* Graph in C */
#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

class comparator {
   public:
    int operator()(const pair<int, int> &p1, const pair<int, int> &p2) {
        return p1.first > p2.first;
    }
};

typedef struct Graph {
    int numVertices;
    struct Node **Adj;

    Graph(int v) {
        Adj = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = dest;
        newNode->weight = w;
        newNode->next = Adj[src];
        Adj[src] = newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = Adj[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
    vector<int> runDijkstra(int src) {
        vector<int> d(numVertices);
        for (auto &x : d) {
            x = 999;
        }
        d[src] = 0;
        //<distance from src,vertex number>
        priority_queue<pair<int, int>, vector<pair<int, int>>, comparator> pq;
        for (int i = 0; i < numVertices; i++) {
            pq.push({d[i], i});
        }
        while (!pq.empty()) {
            auto u = pq.top();
            pq.pop();
            int minVertex = u.second;
            Node *neighbor = Adj[minVertex];
            while (neighbor != NULL) {
                if (d[minVertex] + neighbor->weight < d[neighbor->vertex]) {
                    d[neighbor->vertex] = d[minVertex] + neighbor->weight;
                    pq.push({d[neighbor->vertex], neighbor->vertex});
                }
                neighbor = neighbor->next;
            }
        }

        return d;
    }
} graph;

int main() {
    int v = 5;
    vector<vector<int>> edges = {{0, 1, 2},  {0, 3, 14}, {1, 2, 5}, {1, 3, 10},
                                 {1, 4, 20}, {2, 4, 7},  {3, 4, 15}};
    graph g(v);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }

    g.printGraph();
    int src = 0;
    vector<int> distance = g.runDijkstra(src);
    cout << "Min distance to all vertices from src 0" << endl;
    for (int i = 0; i < distance.size(); i++) {
        cout << src << "->" << i << ": " << distance[i] << endl;
    }
    return 0;
}
//  Adjacency list
// [0]: 3, 1, 
// [1]: 4, 3, 2, 
// [2]: 4, 
// [3]: 4, 
// [4]: 
// Min distance to all vertices from src 0
// 0->0: 0
// 0->1: 2
// 0->2: 7
// 0->3: 12
// 0->4: 14