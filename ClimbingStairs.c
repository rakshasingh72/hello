// TC: O(n)  Space:O(n)

int climbStairs(int n) {
    if (n < 3) return n;

    int c[n + 1];
    c[0] = 0;
    c[1] = 1;
    c[2] = 2;

    for (int i = 3; i <= n; i++) {
        c[i] = c[i - 1] + c[i - 2];
    }
    return c[n];
}

// Recursive Code
// TC:O(2^n)  Space:O(n) Tree height=n

int climbStairs(int n) {
    if (n == 1) return 1;

    if (n == 2) return 2;

    return climbStairs(n - 1) + climbStairs(n - 2);
}

// TC:O(n)  Space:O(n)

class Solution {
    unordered_map<int, int> mem;

   public:
    int climbStairs(int n) {
        if (n == 1) return 1;

        if (n == 2) return 2;

        if (mem.find(n) != mem.end()) {
            return mem[n];
        }

        mem[n] = climbStairs(n - 1) + climbStairs(n - 2);

        return mem[n];
    }
};