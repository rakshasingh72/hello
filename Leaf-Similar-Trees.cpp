// TC: O(n+m)  Space: O(height) recursion
// Link: https://leetcode.com/problems/leaf-similar-trees/
class Solution {
   public:
    bool leafSimilar(TreeNode* root1, TreeNode* root2) {
        string s1 = "";
        string s2 = "";
        getLeaves(root1, s1);
        getLeaves(root2, s2);
        return s1 == s2;
    }

    void getLeaves(TreeNode* root, string& s) {
        if (root == NULL) return;

        if (root->left == NULL && root->right == NULL)
            s += to_string(root->val) + "/";
        getLeaves(root->left, s);
        getLeaves(root->right, s);
    }
};