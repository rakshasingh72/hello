// TC:O(n)  Space:O(n)
// Link: https://leetcode.com/problems/house-robber/

class Solution {
    unordered_map<int, int> mem;

   public:
    int rob(vector<int>& A) {
        int size = A.size();
        return f(A, 0, size - 1);
    }
    int f(vector<int>& A, int i, int n) {
        if (i > n) return 0;
        if (mem.find(i) != mem.end()) {
            return mem[i];
        }
        mem[i] = max(A[i] + f(A, i + 2, n), f(A, i + 1, n));

        return mem[i];
    }
};