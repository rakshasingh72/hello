// TC: O(nlogn) n=length of bigger array Space: O(1)
// Link: https://leetcode.com/problems/intersection-of-two-arrays-ii/

class Solution {
   public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        int n1 = nums1.size();
        int n2 = nums2.size();
        int n, i = 0, j = 0;
        vector<int> ret;
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());
        if (n1 < n2) {
            n = n1;
        } else {
            n = n2;
        }
        while (i < n1 && j < n2) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums2[j] < nums1[i]) {
                j++;
            } else {
                ret.push_back(nums1[i]);
                i++;
                j++;
            }
        }
        return ret;
    }
};