#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

typedef struct Queue {
    int front;
    int rear;
    int capacity;
    int *A;

    void queue_init(int size) {
        A = (int *)malloc(sizeof(int) * size);
        if (A == NULL) {
            printf("error in allocating memory\n");
            return;
        }
        front = -1;
        rear = -1;
        capacity = size;
    }
    Queue() {
        queue_init(5);  // default size
    }
    Queue(int size) { queue_init(size); }
    void enqueue(int val) {
        if ((rear + 1) % capacity == front) {
            // printf("Queue full \n");
            return;
        }
        if (front == -1) {
            front++;
        }
        rear = (rear + 1) % capacity;
        A[rear] = val;
    }
    int dequeue() {
        if (front == -1) {
            //  printf("Queue empty \n");
            return -1;
        }
        if (front == rear) {
            front = -1;
            rear = -1;
            return 0;
        }
        front = (front + 1) % capacity;
        return 0;
    }
    int get_front() {
        if (front == -1) {
            //  printf("Queue empty \n");
            return -1;
        }
        return A[front];
    }
    int get_rear() {
        if (rear == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return A[rear];
    }
    void show() {
        if (front == -1) {
            printf("Queue empty \n");
            return;
        }
        printf("Current queue:  ");
        if (rear >= front) {
            for (int i = front; i <= rear; i++) {
                printf("%d  ", A[i]);
            }
            printf("\n");
        } else {
            for (int i = front; i < capacity; i++) {
                printf("%d  ", A[i]);
            }
            for (int i = 0; i <= rear; i++) {
                printf("%d  ", A[i]);
            }
            printf("\n");
        }
    }
    int isEmpty() {
        if (front == -1) {
            return 1;
        }
        return 0;
    }
} queue;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

typedef struct Graph {
    int numVertices; 
    struct Node **A;

    Graph(int v) {
        A = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest);
        newNode->next = A[src];
        newNode->weight = w;
        A[src] = newNode;
    }
    struct Node *createNode(int v) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = v;
        newNode->next = NULL;
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = A[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
    vector<int> minDistanceByBfs(int src) {
        int visited[numVertices];
        Queue q(numVertices);
        // <vertex,distance from src>
        unordered_map<int, int> *mapPtr = new unordered_map<int, int>;
        memset(visited, 0, numVertices * sizeof(int));
        mapPtr->insert({src, 0});
        bfsHelper(src, visited, &q, *mapPtr);
        for (int i = 0; i < numVertices; i++) {
            if (visited[i] == 0) {
                mapPtr->insert({i, -1});
            }
        }
        return mapToVector(*mapPtr);
    }
    vector<int> mapToVector(unordered_map<int, int> &m) {
        vector<int> vec(numVertices);

        for (auto itr = m.begin(); itr != m.end(); itr++) {
            int vertex = itr->first;
            vec[vertex] = (itr->second);
        }
        return vec;
    }
    void bfsHelper(int v, int visited[], Queue *q, unordered_map<int, int> &m) {
        visited[v] = 1;
        unordered_map<int, int>::iterator itr = m.find(v);
        int count = itr->second;
        struct Node *neighbor = A[v];
        // visiting children of v
        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                m.insert({neighbor->vertex, count + 1});
                visited[neighbor->vertex] = 1;
                q->enqueue(neighbor->vertex);
            }
            neighbor = neighbor->next;
        }
        if (!q->isEmpty()) {
            int child = q->get_front();
            q->dequeue();
            bfsHelper(child, visited, q, m);
        }
    }
} graph;

int main() {
    int v = 5;
    vector<vector<int>> edges = {{0, 1, 1}, {0, 3, 1}, {1, 2, 1}, {1, 3, 1},
                                 {1, 4, 1}, {2, 4, 1}, {3, 4, 1}};

    graph g(v);

    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }
    g.printGraph();

    vector<int> vec = g.minDistanceByBfs(0);

    cout << "Min distance to all vertices from src 0" << endl;
    int i = 0;
    for (auto x : vec) {
        cout << "vertex " << i++ << ":  " << x << endl;
    }
    return 0;
}
//  Adjacency list
// [0]: 3, 1, 
// [1]: 4, 3, 2, 
// [2]: 4, 
// [3]: 4, 
// [4]: 
// Min distance to all vertices from src 0
// vertex 0:  0
// vertex 1:  1
// vertex 2:  2
// vertex 3:  1
// vertex 4:  2