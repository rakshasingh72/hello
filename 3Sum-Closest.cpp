// TC: O(n^2) Space: O(1)
// Link: https://leetcode.com/problems/3sum-closest/

class Solution {
   public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int size = nums.size();
        int ans = nums[0] + nums[1] + nums[size - 1];
        for (int i = 0; i < size - 2; i++) {
            int l = i + 1;
            int r = size - 1;
            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];
                if (sum == target) {
                    return target;
                } else if (sum < target) {
                    l++;
                } else {
                    r--;
                }
                if (abs(target - sum) < abs(target - ans)) ans = sum;
            }
        }
        return ans;
    }
};