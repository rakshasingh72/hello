// TC:O(n)  Space:O(1)
// Link: https://leetcode.com/problems/linked-list-cycle/
bool hasCycle(struct ListNode *head) {
    struct ListNode *p = head;
    struct ListNode *q = head;

    while (p && q && q->next) {
        p = p->next;
        q = q->next->next;

        if (p == q) return true;
    }
    return false;
}