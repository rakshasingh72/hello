// TC: O(n)  Space:O(height) recursion
// Link: https://leetcode.com/problems/path-sum/
class Solution {
   public:
    bool hasPathSum(TreeNode* root, int sum) {
        int pathSum = 0;

        return inorder(root, pathSum, sum);
    }

    bool inorder(TreeNode* root, int ps, int sum) {
        if (root) {
            ps += root->val;
            if (root->left == NULL && root->right == NULL) {
                if (ps == sum) return true;
            }

            return inorder(root->left, ps, sum) ||
                   inorder(root->right, ps, sum);
        }
        return false;
    }
};