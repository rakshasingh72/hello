// TC : O(n^2)  Space: O(n^2) matrix
// Link: https://leetcode.com/problems/palindromic-substrings/

class Solution {
   public:
    int check(string &S, int start, int end, vector<vector<int>> &T) {
        if (start > end) {
            return 1;
        }
        if (T[start][end] == -1) {
            T[start][end] =
                (S[start] == S[end]) && check(S, start + 1, end - 1, T);
        }
        return T[start][end];
    }
    int countSubstrings(string S) {
        int n = S.length();
        vector<vector<int>> T(n, vector<int>(n, -1));

        int cnt = 0;
        for (int len = 1; len <= n; len++) {
            for (int start = 0; start <= n - len; start++) {
                int end = start + len - 1;
                if (check(S, start, end, T)) {
                    cnt++;
                }
            }
        }
        return cnt;
    }
};