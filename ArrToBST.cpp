// TC:O(n) n=array size     Space:O(height)
// Link:https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/

class Solution {
   public:
    TreeNode* helper(int l, int h, vector<int>& A) {
        if (l > h) {
            return NULL;
        }

        int mid = (l + h) / 2;
        TreeNode* root = new TreeNode(A[mid]);
        // struct TreeNode *root=(struct TreeNode*) malloc(sizeof(struct
        // TreeNode));
        root->val = A[mid];
        root->left = helper(l, mid - 1, A);
        root->right = helper(mid + 1, h, A);

        return root;
    }
    TreeNode* sortedArrayToBST(vector<int>& nums) {
        int l = 0, h = nums.size() - 1;
        return helper(l, h, nums);
    }
};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */