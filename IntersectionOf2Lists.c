// TC: O(n) n=length of longer list  Space:O(1)
// Link:  https://leetcode.com/problems/intersection-of-two-linked-lists/

struct ListNode *getIntersectionNode(struct ListNode *headA,
                                     struct ListNode *headB) {
    struct ListNode *p1 = headA;
    struct ListNode *p2 = headB;
    int a = 0, b = 0, n;

    while (p1) {
        a++;
        p1 = p1->next;
    }
    while (p2) {
        b++;
        p2 = p2->next;
    }
    if (a > b) {
        n = a - b;
        while (n && headA) {
            headA = headA->next;
            n--;
        }
    } else {
        n = b - a;
        while (n && headB) {
            headB = headB->next;
            n--;
        }
    }

    while (headA && headB) {
        if (headA == headB)
            return headA;
        else {
            headA = headA->next;
            headB = headB->next;
        }
    }
    return NULL;
}