// TC:O(n) (n is size of string)   SC:O(1)
// Link: https://leetcode.com/problems/roman-to-integer/submissions/

class Solution {
   public:
    int romanToInt(string s) {
        int sum = 0;
        for (int i = 0; s[i] != NULL; i++) {
            switch (s[i]) {
                case 'I':
                    switch (s[i + 1]) {
                        case 'V':
                            sum += 4;
                            i++;
                            break;
                        case 'X':
                            sum += 9;
                            i++;
                            break;
                        default:
                            sum += 1;
                            break;
                    }
                    break;
                case 'V':
                    sum += 5;
                    break;
                case 'X':
                    switch (s[i + 1]) {
                        case 'L':
                            sum += 40;
                            i++;
                            break;
                        case 'C':
                            sum += 90;
                            i++;
                            break;
                        default:
                            sum += 10;
                            break;
                    }
                    break;
                case 'L':
                    sum += 50;
                    break;
                case 'C':
                    switch (s[i + 1]) {
                        case 'D':
                            sum += 400;
                            i++;
                            break;
                        case 'M':
                            sum += 900;
                            i++;
                            break;
                        default:
                            sum += 100;
                            break;
                    }
                    break;
                case 'D':
                    sum += 500;
                    break;
                case 'M':
                    sum += 1000;
                    break;
            }
        }
        return sum;
    }
};