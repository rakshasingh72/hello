// TC: O(n) n=length of string   Space: O(1)
// Link: https://leetcode.com/problems/valid-anagram/

bool isAnagram(char* s, char* t) {
    int Sfreq[26];
    int Tfreq[26];
    int j = 0;
    for (int i = 0; i < 26; i++) {
        Sfreq[i] = 0;
        Tfreq[i] = 0;
    }
    while (s[j] != '\0' && t[j] != '\0') {
        Sfreq[s[j] - 'a']++;
        Tfreq[t[j] - 'a']++;
        j++;
    }
    if (t[j] != '\0' || s[j] != '\0') return false;
    for (int i = 0; i < 26; i++) {
        if (Sfreq[i] != Tfreq[i]) return false;
    }
    return true;
}