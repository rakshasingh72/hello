// TC: O(n) Space: O(height) recursion
// Link: https://leetcode.com/problems/range-sum-of-bst/

class Solution {
   public:
    int rangeSumBST(TreeNode* root, int L, int R) {
        int sum = 0;
        if (root == NULL) return sum;

        getSum(root, L, R, sum);

        return sum;
    }
    void getSum(TreeNode* root, int L, int R, int& sum) {
        if (root == NULL) return;

        if (root->val >= L && root->val <= R) sum += root->val;

        if (L < root->val) getSum(root->left, L, R, sum);
        if (R > root->val) getSum(root->right, L, R, sum);
    }
};