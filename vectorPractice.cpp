#include <iostream>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

using namespace std;

template <class T>
void printVector(vector<T> v)
{
    cout << "Vector elements: ";
    for (int i = 0; i < v.size(); i++)
        cout << v[i] << "  ";
    cout << endl;
}
void vectorDemo(vector<int> v)
{
    cout << "v size:" << v.size() << endl;
    cout << "pushed 100" << endl;
    v.push_back(100);
    cout << "first element=" << v.front() << endl
         << "last element=" << v.back() << endl;
    cout << "pop_back" << endl;
    v.pop_back();
    cout << "Last element=" << v.back() << endl;
}
void vectorConcat(vector<int> &v1, vector<int> &v2)
{

    v1.insert(v1.end(), v2.begin(), v2.end());
}
void vectorSum(vector<int> v)
{
    int sum = 0;
    accumulate(v.begin(), v.end(), sum);
    cout << "Vector sum=" << sum << endl;
}
int compare(int x, int y)
{
    return x > y;
}
int compareByLen(string s1, string s2)
{
    return s1.size() < s2.size();
}
void vectorQuestion()
{
  //1.Create a vector with 10 random integer and increment each element by 1. 
   
    vector<int> v1 = {10, 11,12,13,14,15,6,7,8,9};

    printVector(v1);

    int &x = v1.front();

    for (int i = 0; i < v1.size(); i++)
        v1[i]++;

    cout << "After incrementing each element by 1" << endl;
    printVector(v1);

    //2.Concatenate two vectors of ints.
    vector<int> v2 = {40, 50};

    vectorConcat(v1, v2);
    cout << "After concatenating v1,v2 " << endl;
    printVector(v1);

//3.Sum all elements.
    vectorSum(v1);

/*
4.Create a vector with 10 random integer and use STL functions only to perform the following 
  operations on the vector. 
 a) find the max and min elements. 
 b) Reverse this vector. 
 c) Erase integer 3rd element from last 
 d) sort this vector in increasing order. 
 e) Test if a number (say 15) is present in this vector using Binary search algorithm
  (hint: user lower_bound / upper_bound) 
 f) insert 2 elements (4 and 10) at 3rd position from last. 
 g) d) sort this vector in decreasing order. 
 h) Test if 17 is present. i) swap first and last integer
 */
    vector<int> v = {14, 2, 13, 5, 66, 7};
    printVector(v);

    vector<int>::iterator it1 = max_element(v.begin(), v.end());
    vector<int>::iterator it2 = min_element(v.begin(), v.end());
    cout << "Max=" << *it1 << "  Min" << *it2 << endl;

    sort(v.begin(), v.end());
    cout << "After sorting" << endl;
    printVector(v);

    cout << "After Reverse 0 to 2 idx" << endl;
    reverse(v.begin(), v.begin() + 3);
    printVector(v);

    cout << "Erasing 3rd element from last" << endl;
    v.erase(v.end() - 3);
    printVector(v);

    sort(v.begin(), v.end());
    bool keyFound = binary_search(v.begin(), v.end(), 14);
    cout << "key 14 found=" << keyFound << endl;

    cout << "Adding 4,10 at position 3" << endl;
    vector<int>::iterator it = v.insert(v.begin() + 3, 4);
    v.insert(it, 10);
    printVector(v);

    sort(v.begin(), v.end(), compare);
    cout << "After sorting" << endl;
    printVector(v);

    cout << "Swapping first and last element" << endl;
    //iter_swap(v.begin(), v.end() - 1);
    swap(v.front(), v.back());
    printVector(v);

//5.How to get a direct pointer to the memory array used internally by vector?
    int *ptr = v.data();
    cout << "*(++ptr)" << *(++ptr) << endl;

//6.Create a vector of 10 random strings and use STL functions only to perform 
// sorting by string length
    vector<string> str = {"raksha", "tony", "poony"};
    sort(str.begin(), str.end(), compareByLen);
    cout << "String sort\n";
    for (string i : str)
    {
        cout << i << "  " << endl;
    }
}
void printSet(unordered_set<int> us)
{
    cout << "Unordered_set: ";
    for (auto itr = us.begin(); itr != us.end(); itr++)
    {
        cout << *itr << "  ";
    }
    cout << "\n set size=" << us.size() << endl;
}
void unorderedSet()
{
    // 1. Create an unordred set of 10 random ints.
    unordered_set<int> intSet;

    srand(time(0));

    for (int i = 0; i < 10; i++)
    {
        intSet.insert(rand() % 100);
    }
    printSet(intSet);

    //2.Insert an element, delete an element, Test the presence of an element.
    //Find max and min elements.
    unordered_set<int> s1 = {2, 4, 5, 6, 7};
    s1.insert(100);
    s1.erase(5);
    printSet(s1);
    unordered_set<int>::iterator keyPtr = s1.find(100);

    if (keyPtr == s1.end())
        cout << "Not found\n";
    else
        cout << "Found " << *keyPtr << endl;

    cout << "Max element= " << *max_element(s1.begin(), s1.end()) << endl;
    cout << "Min element= " << *min_element(s1.begin(), s1.end()) << endl;

    //3.What underlying data structure is used?
    //What is the time complexity of insert, delete, check, max, and min operations?
    //DS=Hashtable
    //TC:O(1) for insert,delete,check
    //TC:O(n) for min,max
}
int main()
{
    //vectorQuestion();

    //STL: unordered set
    unorderedSet();
}