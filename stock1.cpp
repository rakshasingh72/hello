// TC: O(n)  Spcae:O(1)
// Link: https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

class Solution {
   public:
    int maxProfit(vector<int>& prices) {
        int maxCurPro = 0;
        int minCurBuy = INT_MAX;
        for (int i = 0; i < prices.size(); i++) {
            minCurBuy = min(prices[i], minCurBuy);
            maxCurPro = max(maxCurPro, prices[i] - minCurBuy);
        }
        return maxCurPro;
    }
};