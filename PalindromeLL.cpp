// TC: O(n)  Space:O(n)
// Link: https://leetcode.com/problems/palindrome-linked-list/

class Solution {
   public:
    bool isPalindrome(ListNode* head) {
        ListNode* temp = head;
        int count = 0;
        while (temp) {
            count++;
            temp = temp->next;
        }
        if (count == 0 || count == 1) return true;
        stack<int> s;
        int n = count / 2;
        while (n) {
            s.push(head->val);
            head = head->next;
            n--;
        }
        if (count % 2 == 1) head = head->next;      // Odd palindrome

        while (head) {
            if (s.empty()) return false;
            if (s.top() != head->val)
                return false;
            else
                s.pop();
            head = head->next;
        }
        if (s.empty())
            return true;
        else
            return false;
    }
};