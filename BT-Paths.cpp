// TC : O(n)  Space:O(height) recursion
// Link : https://leetcode.com/problems/binary-tree-paths/

class Solution {
   public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> ret;
        if (root == NULL) return ret;
        traverse(root, "", ret);
        return ret;
    }

    void traverse(TreeNode* root, string path, vector<string>& ret) {
        if (root == NULL) {
            return;
        }
        if (root->left == NULL && root->right == NULL) {
            ret.push_back(path + to_string(root->val));
            return;
        }
        traverse(root->left, path + to_string(root->val) + "->", ret);
        traverse(root->right, path + to_string(root->val) + "->", ret);
    }
};