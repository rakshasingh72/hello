// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/univalued-binary-tree/
class Solution {
   public:
    bool isUnivalTree(TreeNode* root) {
        if (root == NULL) return true;
        return dfs(root, root->val);
    }

    bool dfs(TreeNode* root, int val) {
        if (root == NULL) return true;

        return root->val == val && dfs(root->left, val) &&
               dfs(root->right, val);
    }
};