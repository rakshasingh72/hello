// TC: O(n)  Space:O(n) queue
// Link: https://leetcode.com/problems/binary-tree-level-order-traversal/

class Solution {
   public:
    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> ret;
        if (root == NULL) return ret;
        queue<TreeNode*> Q;
        Q.push(root);
        int levelNodeCnt = 0;
        while (!Q.empty()) {
            levelNodeCnt = Q.size();
            ret.push_back(vector<int>());
            for (int i = 0; i < levelNodeCnt; i++) {
                if (Q.front()->left) Q.push(Q.front()->left);
                if (Q.front()->right) Q.push(Q.front()->right);
                ret.back().push_back(Q.front()->val);
                Q.pop();
            }
        }
        return ret;
    }
};