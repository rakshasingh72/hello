// TC:O(n)   Space:O(height) worst case:O(n) (skewed tree)
// Link: https://leetcode.com/problems/symmetric-tree/

bool isSame(struct TreeNode* p, struct TreeNode* q) {
    if (!p && !q)
        return true;
    else if (!p && q)
        return false;
    else if (p && !q)
        return false;

    return (p->val == q->val) && isSame(p->left, q->right) &&
           isSame(p->right, q->left);
}
bool isSymmetric(struct TreeNode* root) {
    if (!root) return true;
    return isSame(root->left, root->right);
}
