// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/construct-string-from-binary-tree/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
   public:
    string tree2str(TreeNode* t) {
        string str = "";

        return makeStr(t, str);
    }

    string makeStr(TreeNode* root, string& str) {
        if (root == NULL) return str;
        str += to_string(root->val);
        if (root->left == NULL && root->right == NULL) return str;
        if (root->left == NULL && root->right != NULL) {
            str += "()";
        } else {
            str = str + '(';
            str = makeStr(root->left, str) + ')';
        }
        if (root->right != NULL) {
            str = str + '(';
            str = makeStr(root->right, str) + ')';
        }

        return str;
    }
};