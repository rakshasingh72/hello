//TC:O(1)  Space:O(1)
//Link:https://leetcode.com/problems/reverse-integer/submissions/

class Solution
{
  public:
    int reverse(int x)
    {
        long int r = 0, ret;
        unsigned int n = abs((long int)x);

        while (n)
        {
            r = r * 10 + n % 10;
            n = n / 10;
        }

        ret = x < 0 ? -r : r;

        if (ret < -(1L << 31) || ret > ((1L << 31) - 1))
        {
            return 0;
        }

        return ret;
    }
};
