
// TC: O(n)  Space: O(n) Queue
// Link: https://leetcode.com/problems/average-of-levels-in-binary-tree/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
   public:
    vector<double> averageOfLevels(TreeNode* root) {
        vector<double> ans;
        if (root == NULL) return ans;
        queue<TreeNode*> Q;
        Q.push(root);
        while (!Q.empty()) {
            double avg = 0;
            double sum = 0;
            int levelNodeCnt = Q.size();
            for (int i = 0; i < levelNodeCnt; i++) {
                if (Q.front()->left) Q.push(Q.front()->left);
                if (Q.front()->right) Q.push(Q.front()->right);
                sum += Q.front()->val;
                Q.pop();
            }
            avg = sum / levelNodeCnt;
            ans.push_back(avg);
        }
        return ans;
    }
};