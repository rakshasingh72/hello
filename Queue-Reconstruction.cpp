// TC: O(n^2)  Space:O(1)
// Link: https://leetcode.com/problems/queue-reconstruction-by-height/

class Solution {
   public:
    struct sort_pred {
        bool operator()(const vector<int> &left, const vector<int> &right) {
            if (left[0] == right[0]) return left[1] < right[1];

            return left[0] > right[0];
        }
    };
    vector<vector<int>> reconstructQueue(vector<vector<int>> &P) {
        sort(P.begin(), P.end(),
             sort_pred());  // sort by height in decreasing order

        for (int i = 0; i < P.size(); i++) {
            if (P[i][1] != i) {
                vector<int> tmp = P[i];
                P.erase(P.begin() + i);
                P.insert(P.begin() + tmp[1], tmp);
            }
        }
        return P;
    }
};