// TC: O(n) space:O(height)
// Link: https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
class Solution {
   public:
    vector<vector<int>> levelOrderBottom(TreeNode* root) {
        vector<vector<int>> ret;
        dfs(root, ret, 0);
        reverse(ret.begin(), ret.end());
        return ret;
    }
    void dfs(TreeNode* root, vector<vector<int>>& ret, int level) {
        if (root == NULL) return;
        if (level == ret.size()) ret.push_back({});
        ret[level].push_back(root->val);
        dfs(root->left, ret, level + 1);
        dfs(root->right, ret, level + 1);
    }
};