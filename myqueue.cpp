#include <stdio.h>
#include <stdlib.h>

typedef struct Queue {
    int front;
    int rear;
    int capacity;
    int *arr;

    void queue_init(int size) {
        arr = (int *)malloc(sizeof(int) * size);
        if (arr == NULL) {
            printf("error in allocating memory\n");
            return;
        }
        front = -1;
        rear = -1;
        capacity = size;
    }
    Queue() {
        queue_init(5);  // default size
    }
    Queue(int size) { queue_init(size); }
    void enqueue(int val) {
        if ((rear + 1) % capacity == front) {
            printf("Queue full \n");
            return;
        }
        if (front == -1) {
            front++;
        }
        rear = (rear + 1) % capacity;
        arr[rear] = val;
    }
    int dequeue() {
        if (front == -1) {
            printf("Queue empty \n");
            return -1;
        }
        if (front == rear) {
            front = -1;
            rear = -1;
            return 0;
        }
        front = (front + 1) % capacity;
        return 0;
    }
    int get_front() {
        if (front == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return arr[front];
    }
    int get_rear() {
        if (rear == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return arr[rear];
    }
    void show() {
        if (front == -1) {
            printf("Queue empty \n");
            return;
        }
        printf("Current queue:  ");
        if (rear >= front) {
            for (int i = front; i <= rear; i++) {
                printf("%d  ", arr[i]);
            }
            printf("\n");
        } else {
            for (int i = front; i < capacity; i++) {
                printf("%d  ", arr[i]);
            }
            for (int i = 0; i <= rear; i++) {
                printf("%d  ", arr[i]);
            }
            printf("\n");
        }
    }
    int isEmpty() {
        if (front == -1) {
            return 1;
        }
        return 0;
    }
} queue;

int main() {
    queue q;
    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    q.dequeue();
    q.enqueue(4);
    q.enqueue(5);
    q.dequeue();
    q.show();  // Current queue:  3  4  5  6  7
    printf("front=%d  rear=%d\n ", q.get_front(),
           q.get_rear());  // front=3  rear=5
    return 0;
}