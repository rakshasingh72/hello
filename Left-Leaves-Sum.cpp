// TC: O(n) Space:O(height) recursion
// Link: https://leetcode.com/problems/sum-of-left-leaves/

class Solution {
   public:
    int sumOfLeftLeaves(TreeNode* root) {
        int sum = 0;
        if (root == NULL) return 0;
        return dfs(root, sum, -1);
    }
    int dfs(TreeNode* root, int& sum, int child) {
        if (root == NULL) return sum;
        if (root->left == NULL && root->right == NULL && child == 0)
            sum += root->val;
        dfs(root->left, sum, 0);
        dfs(root->right, sum, 1);

        return sum;
    }
};