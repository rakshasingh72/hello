// TC: O(n)  Space:O(1)
// Link: https://leetcode.com/problems/rotate-array/

class Solution {
   public:
    void rotate(vector<int>& nums, int k) {
        int n = nums.size();
        reverse(nums, nums + (n - k));
        reverse(nums + n - k, nums + n);
        reverse(nums, nums + n);
    }
};