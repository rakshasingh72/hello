// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/path-sum-ii/
class Solution {
   public:
    vector<vector<int>> pathSum(TreeNode* root, int sum) {
        vector<vector<int>> ret;
        vector<int> path;
        return findPath(root, sum, path, ret);
    }
    vector<vector<int>> findPath(TreeNode* root, int sum, vector<int> path,
                                 vector<vector<int>>& ret) {
        if (root == NULL) {
            return ret;
        }
        path.push_back(root->val);
        if (root->left == NULL && root->right == NULL) {
            if (sum - root->val == 0) {
                ret.push_back(path);
            } else {
                path.pop_back();
            }
        }
        findPath(root->left, sum - root->val, path, ret);
        findPath(root->right, sum - root->val, path, ret);

        return ret;
    }
};