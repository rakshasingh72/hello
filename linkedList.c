#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int val;
    struct Node *next;
};
struct Node *createList(int n)
{
    struct Node *head = malloc(sizeof(struct Node));
    head->val = 0;
    head->next = NULL;
    struct Node *cur = head;
    n--;
    while (n > 0)
    {
        struct Node *new = malloc(sizeof(struct Node));
        new->val = 0;
        new->next = NULL;
        cur->next = new;
        cur = new;
        n--;
    }
    struct Node *c = head;
    while (c->next == NULL)
    {
        c = c->next;
    }

    return head;
}
struct Node *addFront(struct Node *head, int data)
{
    struct Node *newHead = malloc(sizeof(struct Node));
    newHead->val = data;
    newHead->next = head;

    return newHead;
}
struct Node *addBack(struct Node *head, int data)
{
    struct Node *new = malloc(sizeof(struct Node));
    new->val = data;
    new->next = NULL;

    if (head == NULL)
        return new;

    struct Node *cur = head;
    while (cur->next != NULL)
    {
        cur = cur->next;
    }
    cur->next = new;

    return head;
}
struct Node *deleteNthNode(struct Node *head, int n)
{
    struct Node *cur = head;
    struct Node *prev = NULL;
    struct Node *x = NULL;
    if (n == 1) // update head if first node deleted
    {
        x = head;
        head = cur->next;
        free(x);
        return head;
    }
    n = n - 2;
    while (n > 0)
    {
        if (cur == NULL)
            break;
        prev = cur;
        cur = cur->next;
        n--;
    }
    if (cur == NULL)
    {
        printf("List exhausted\n");
        return head;
    }

    if (cur->next == NULL) // To delete last node
    {
        x = prev->next;
        prev->next = NULL;
    }
    else
    {
        x = cur->next;
        cur->next = cur->next->next;
    }
    free(x);

    return head;
}
void printList(struct Node *head)
{
    struct Node *cur = head;
    printf("Elements of current List\n");
    while (cur != NULL)
    {
        printf("%d->", cur->val);
        cur = cur->next;
    }
    printf("NULL\n");
}
int updateNthNode(struct Node *head, int n, int data)
{
    struct Node *cur = head;

    n = n - 1;
    while (n > 0)
    {
        if (cur == NULL) break;
        cur = cur->next;
        n--;
    }
    if (cur == NULL)
    {
        printf("List exhausted\n");
        return 0;
    }
    cur->val = data;

    return 1;
}
int main()
{
    struct Node *head = createList(2); //0  0

    head = addFront(head, 12);                //12  0  0
    head = addFront(head, 13);                //13  12  0  0
    head = addBack(head, 50);                 //13  12  0  0 50
    head = deleteNthNode(head, 5);            //13  12  0  0
    int updated = updateNthNode(head, 3, 14); //13 12 14 0
    printList(head);
}