// TC: O(n)  Space:O(1)
// Link: https://leetcode.com/problems/linked-list-cycle-ii/
class Solution {
   public:
    ListNode* detectCycle(ListNode* head) {
        ListNode* slow = head;
        ListNode* fast = head;
        bool cycle = false;
        while (fast && fast->next) {
            slow = slow->next;
            fast = fast->next->next;
            if (slow == fast) {
                cycle = true;
                break;
            }
        }
        if (cycle == true) {
            while (head != slow) {
                head = head->next;
                slow = slow->next;
            }
            return slow;
        } else {
            return NULL;
        }
    }
};