// TC: O(n sqrt(n)) Space:O(1)
// Link: https://leetcode.com/problems/count-primes/

int countPrimes(int n) {
    int c = 0;
    for (int i = 2; i < n; i++) {
        int p = 1;
        for (int j = 2; j <= sqrt(i); j++) {
            if (i % j == 0) {
                p = 0;
                break;
            }
        }
        if (p == 1) c++;
    }
    return c;
}