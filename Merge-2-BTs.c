// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/merge-two-binary-trees/

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
struct TreeNode* mergeTrees(struct TreeNode* t1, struct TreeNode* t2) {
    if (!t1 && !t2) return NULL;

    struct TreeNode* tmp = malloc(sizeof(struct TreeNode));

    if (!t1 | !t2) tmp->val = t1 ? t1->val : t2->val;

    if (t1 && t2) tmp->val = t1->val + t2->val;

    tmp->left = mergeTrees(t1 ? t1->left : NULL, t2 ? t2->left : NULL);

    tmp->right = mergeTrees(t1 ? t1->right : NULL, t2 ? t2->right : NULL);

    return tmp;
}