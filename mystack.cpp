#include <stdio.h>
#include <stdlib.h>

typedef enum StatusCode {
    SUCCESS = 0,
    STACK_UNDERFLOW_ERROR = -1,
    STACK_OVERFLOW_ERROR = -2,
} StatusCode;

typedef struct Stack {
    int *arr;
    int top;
    int capacity;

    void stack_init(int size) {
        arr = (int *)malloc(sizeof(int) * size);
        if (arr == NULL) {
            printf("error in allocating memory\n");
            return;
        }
        top = -1;
        capacity = size;
    }
    Stack() { stack_init(5); }
    Stack(int size) { stack_init(size); }
    int push(int val) {
        top++;
        if (top >= capacity) {
            printf("Stack overflow \n");
            return STACK_OVERFLOW_ERROR;
        }
        arr[top] = val;
        return SUCCESS;
    }
    int pop() {
        if (top < 0) {
            printf("Stack underflow \n");
            return STACK_UNDERFLOW_ERROR;
        }
        return arr[top--];
    }
    int getTop() {
        if (top < 0) {
            printf("Stack underflow \n");
            return STACK_UNDERFLOW_ERROR;
        }
        return arr[top];
    }
    void show() {
        int i = 0;
        printf("Printing bottom to top\n");
        while (i <= top) {
            printf("%d  ", arr[i]);
            i++;
        }
        printf("\n");
    }
    int isEmpty() {
        if (top < 0) {
            return 1;
        }
        return 0;
    }
} stack;

int main() {
    stack s;

    s.push(10);
    s.push(20);
    s.push(14);
    s.push(15);
    s.push(30);
    s.show();
    s.pop();
    s.pop();
    printf("isempty=%d\n", s.isEmpty());
    s.show();
    printf("top=%d\n", s.getTop());
    return 0;
}
/*
Printing bottom to top
10  20  14  15  30
isempty=0
Printing bottom to top
10  20  14
top=14
*/