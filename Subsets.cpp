//TC: O(2^n)  Space: O(n.2^n-1) [1*nC1+2*nC2+3*nC3+...+n*nCn]  Recursion Stack height=n
//Link: https://leetcode.com/problems/subsets/

class Solution {
   public:
    void helper(vector<int> &A, vector<int> &chosen, int i,
                vector<vector<int>> &ret) {
        if (i == A.size()) {
            ret.push_back(chosen);
        } else {
            chosen.push_back(A[i]);
            helper(A, chosen, i + 1, ret);
            chosen.pop_back();
            helper(A, chosen, i + 1, ret);
        }
    }
    vector<vector<int>> subsets(vector<int> &A) {
        vector<vector<int>> ret;
        vector<int> chosen;
        helper(A, chosen, 0, ret);
        return ret;
    }
};