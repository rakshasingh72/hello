// TC: O(n)  Space: O(n) stack
// Link: https://leetcode.com/problems/binary-tree-inorder-traversal/

class Solution {
    vector<int> ret;

   public:
    vector<int> inorderTraversal(TreeNode* root) {
        stack<TreeNode*> S;
        while (1) {
            while (root) {
                S.push(root);
                root = root->left;
            }
            if (S.empty()) break;
            ret.push_back(S.top()->val);
            root = S.top()->right;
            S.pop();
        }
        return ret;
    }
};