#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

typedef struct Queue {
    int front;
    int rear;
    int capacity;
    int *A;

    void queue_init(int size) {
        A = (int *)malloc(sizeof(int) * size);
        if (A == NULL) {
            printf("error in allocating memory\n");
            return;
        }
        front = -1;
        rear = -1;
        capacity = size;
    }
    Queue() {
        queue_init(5);  // default size
    }
    Queue(int size) { queue_init(size); }
    void enqueue(int val) {
        if ((rear + 1) % capacity == front) {
            // printf("Queue full \n");
            return;
        }
        if (front == -1) {
            front++;
        }
        rear = (rear + 1) % capacity;
        A[rear] = val;
    }
    int dequeue() {
        if (front == -1) {
            //  printf("Queue empty \n");
            return -1;
        }
        int dequeueElement = A[front];
        if (front == rear) {
            front = -1;
            rear = -1;
            return dequeueElement;
        }
        front = (front + 1) % capacity;
        return dequeueElement;
    }
    int get_front() {
        if (front == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return A[front];
    }
    int get_rear() {
        if (rear == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return A[rear];
    }
    void show() {
        if (front == -1) {
            printf("Queue empty \n");
            return;
        }
        printf("Current queue:  ");
        if (rear >= front) {
            for (int i = front; i <= rear; i++) {
                printf("%d  ", A[i]);
            }
            printf("\n");
        } else {
            for (int i = front; i < capacity; i++) {
                printf("%d  ", A[i]);
            }
            for (int i = 0; i <= rear; i++) {
                printf("%d  ", A[i]);
            }
            printf("\n");
        }
    }
    int isEmpty() {
        if (front == -1) {
            return 1;
        }
        return 0;
    }
} queue;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

typedef struct Graph {
    int numVertices; 
    struct Node **A;

    Graph(int v) {
        A = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest);
        newNode->next = A[src];
        newNode->weight = w;
        A[src] = newNode;
    }
    struct Node *createNode(int v) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = v;
        newNode->next = NULL;
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = A[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
    void dfs(int v) {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));
        // for all unvisited v call dfsHelper
        for (int i = 0; i < numVertices; i++) {
            if (visited[v] == 0) {
                dfsHelper(v, visited);
            }
            v = (v + 1) % numVertices;
        }
    }
    void dfsHelper(int v, int *visited) {
        visited[v] = 1;
        cout << v << "  ";
        struct Node *neighbor = A[v];
        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                dfsHelper(neighbor->vertex, visited);
            }
            neighbor = neighbor->next;
        }
    }
    void bfs(int v) {
        int visited[numVertices];
        Queue q(numVertices);
        memset(visited, 0, numVertices * sizeof(int));
        for (int i = 0; i < numVertices; i++) {
            if (visited[v] == 0) {
                bfsHelper(v, visited, &q);
            }
            v = (v + 1) % numVertices;
        }
    }
    void bfsHelper(int v, int visited[], Queue *q) {
        visited[v] = 1;
        cout << v << "  ";
        struct Node *neighbor = A[v];

        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                visited[neighbor->vertex] = 1;
                q->enqueue(neighbor->vertex);
            }
            neighbor = neighbor->next;
        }
        v = q->dequeue();
        if (v != -1) {
            bfsHelper(v, visited, q);
        }
    }
} graph;

int main() {
    int v = 5;
    vector<vector<int>> edges = {{0, 1, 1}, {0, 2, 1}, {0, 3, 1}, {1, 0, 1},
                                 {1, 2, 1}, {1, 3, 1}, {2, 0, 1}, {2, 1, 1},
                                 {2, 3, 1}, {3, 0, 1}, {3, 1, 1}, {3, 2, 1}};

    graph g(v);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }

    g.printGraph();
    cout << "DFS order" << endl;
    g.dfs(0);
    cout << "\n BFS Order" << endl;
     g.bfs(3);
    return 0;
}
//  Adjacency list
// [0]: 3, 2, 1, 
// [1]: 3, 2, 0, 
// [2]: 3, 1, 0, 
// [3]: 2, 1, 0, 
// [4]: 
// DFS order
// 0  3  2  1  4  
//  BFS Order
// 3  2  1  0  4 