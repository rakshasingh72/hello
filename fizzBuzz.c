#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char **fizzBuzz(int n, int *returnSize)
{
    char **charArr = (char **)malloc(n * sizeof(char *));
    int s = 0;
    for (int i = 1; i <= n; i++)
    {
        if (i % 3 == 0 && i % 5 == 0)
        {
            charArr[i - 1] = malloc(strlen("FizzBuzz") + 1);
           // printf("t1\n");
            strcpy(charArr[i - 1], "FizzBuzz");
            //s=s+8;
        }
        else if (i % 3 == 0)
        {
            charArr[i - 1] = malloc(strlen("Fizz") + 1);
           // printf("t2\n");
            strcpy(charArr[i - 1], "Fizz");
            //s=s+4;
        }

        else if (i % 5 == 0)
        {
            charArr[i - 1] = malloc(strlen("Buzz") + 1);
   
            strcpy(charArr[i - 1], "Buzz");
            //s=s+4;
        }

        else
        {
           // char* *buf = malloc(4*sizeof(char *));
    
            char buf[11];
            sprintf(buf, "%d", i);
             charArr[i - 1] = malloc(strlen(buf) + 1);
             strcpy(charArr[i-1],buf);

        }
    }

    *returnSize = n;

    return charArr;
}

int main()
{
    int n;
    scanf("%d", &n);
    int size;

    char **strArr = fizzBuzz(n, &size);
       
       for(int i=0;i<n;i++)
       {
           printf("%s \n",strArr[i]);
       }
    // printf("\n");

    return 0;
}
