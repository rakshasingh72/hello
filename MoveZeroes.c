// TC: O(n) Space:O(1)
// Link: https://leetcode.com/problems/move-zeroes/

void swap(int *x, int *y) {
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}
void moveZeroes(int *nums, int numsSize) {
    int i = -1, j = 0, temp;  // all non-zero element on left side of index i
    while (j < numsSize) {
        if (nums[j] != 0) {
            i++;
            swap(&nums[i], &nums[j]);
        }
        j++;
    }
}