// TC:O(nlogn)  Space:O(n)
// Link: https://leetcode.com/problems/shortest-unsorted-continuous-subarray/

class Solution {
   public:
    int findUnsortedSubarray(vector<int>& A) {
        int l = 0, r = -1, i, j;
        int n = A.size();
        vector<int> B = A;
        sort(B.begin(), B.end());
        for (i = 0; i < n; i++) {
            if (A[i] != B[i]) {
                l = i;
                break;
            }
        }
        for (j = n - 1; j >= 0; j--) {
            if (i >= j) break;
            if (j > i && A[j] != B[j]) {
                r = j;
                break;
            }
        }
        return r - l + 1;
    }
};