// TC: O()  Space: O()
// Link: https://leetcode.com/problems/first-unique-character-in-A-string/

int firstUniqChar(char* s) {
    int freqCnt[26];
    int j = 0;

    memset(freqCnt, 0, 26 * sizeof(int));
    while (s[j]) {
        freqCnt[s[j] - 'a']++;
        j++;
    }
    j = 0;
    while (s[j] != NULL) {
        if (freqCnt[s[j] - 'a'] == 1) return j;

        j++;
    }
    return -1;
}