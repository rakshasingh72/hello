#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASH_TABLE_SIZE 100

struct Node {
    char *key;
    int val;
    struct Node *next;
};

typedef struct Hashmap {
    struct Node **A;

    Hashmap() {
        A = (struct Node **)calloc(HASH_TABLE_SIZE, sizeof(struct Node *));
    }
    int hash(char *key) {
        int sum = 0;
        for (int i = 0; key[i] != '\0'; i++) {
            sum += key[i];
        }
        return sum % HASH_TABLE_SIZE;
    }
    void insert(char *k, int v) {
        int idx = hash(k);
        // Add key to front
        Node *newHead = (Node *)malloc(sizeof(struct Node));
        newHead->key = strdup(k);
        newHead->val = v;
        newHead->next = A[idx];
        A[idx] = newHead;
    }
    int getValue(char *k) {
        int idx = hash(k);
        struct Node *cur = A[idx];
        while (cur != NULL) {
            if (strcmp(cur->key, k) == 0) {
                return cur->val;
            }
            cur = cur->next;
        }
        return -1;
    }
    int deleteKey(char *k) {
        int idx = hash(k);
        struct Node *cur = A[idx];
        struct Node *prev = NULL;

        if (cur != NULL && strcmp(cur->key, k) == 0) {  // if found at list head
            A[idx] = cur->next;
            free(cur);
            return 0;
        }
        while (cur != NULL) {
            if (strcmp(cur->key, k) == 0) {
                prev->next = cur->next;
                free(cur);
                return 0;
            }
            prev = cur;
            cur = cur->next;
        }
        printf("%s NOT FOUND \n", k);
        return -1;
    }

} hashmap;

int main() {
    hashmap m;
    char r[] = "Raksha";
    char s[] = "Senior";
    char j[] = "Junior";
    m.insert(r, 14);
    m.insert(s, 200);
    printf("Raksha value=%d \n", m.getValue(r));
    printf("Senior value=%d \n", m.getValue(s));
    printf("Senior Deleted =%d \n", m.deleteKey(s));
    printf("Senior value=%d \n", m.getValue(s));
    printf("Junior Deleted=%d \n ", m.deleteKey(j));
    return 0;
}
/*
Raksha value=14
Senior value=200
Senior Deleted =0
Senior value=-1
Junior NOT FOUND
Junior Deleted=-1
*/
