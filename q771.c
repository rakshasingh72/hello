/*
https://leetcode.com/problems/jewels-and-stones/
T: 
S:

*/
int numJewelsInStones(char *J, char *S)
{

    int Jpresent[52];
    int i = 0, jewels = 0;

    for (int i = 0; i < 52; i++)
        Jpresent[i] = 0;

    while (J[i])
    {

        if ('A' <= J[i] && J[i] <= 'Z')
        {
            if (Jpresent[J[i] - 'A'] != 1)
                Jpresent[J[i] - 'A'] = 1;
        }

        else
        {
            if (Jpresent[J[i] - 'a' + 26] == 0)
                Jpresent[J[i] - 'a' + 26] = 1;
        }

        i++;
    }

    while (*S)
    {
        if ('A' <= *S && *S <= 'Z')
        {
            if (Jpresent[*S - 'A'] == 1)
                jewels++;
        }
        else
        {
            if (Jpresent[*S - 'a' + 26] == 1)
                jewels++;
        }

        S++;
    }

    return jewels;
}
