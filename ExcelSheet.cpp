// TC: O(n) n=length of string, Space: O(1)
// Link: https://leetcode.com/problems/excel-sheet-column-number/

class Solution {
   public:
    int titleToNumber(string s) {
        int n = s.length();
        int num = 0;
        for (int i = 0; i < n; i++) {
            num = 26 * num + (s[i] - 'A' + 1);
        }
        return num;
    }
};