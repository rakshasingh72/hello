#include <bits/stdc++.h>

using namespace std;

struct Vertex {
    int name;
    int key;
    int parent;
    bool operator==(const Vertex& n1) { return n1.name == name; }
};

struct Edge {
    int src;
    int dest;
    int weight;
};

int** get2DMatrix(int r, int c) {
    int** A = (int**)malloc(r * (sizeof(int*)));
    for (int i = 0; i < r; i++) {
        A[i] = (int*)malloc(c * sizeof(int));
    }
    return A;
}

class vertex_comparator {
   public:
    int operator()(const Vertex& x, const Vertex& y) { return x.key > y.key; }
};

class edge_comparator {
   public:
    int operator()(const Edge& x, const Edge& y) { return x.weight < y.weight; }
};

class UnionFind {
    vector<int> parent;
    vector<int> size;

   public:
    UnionFind(int n) {
        parent.resize(n);
        size.resize(n);
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            size[i] = 1;
        }
    }
    int root(int i) {
        while (i != parent[i]) {
            i = parent[i];
            parent[i] = parent[parent[i]];  // path compression
        }
        return i;
    }
    bool isConnected(int p, int q) { return root(p) == root(q); }

    void Union(int p, int q) {
        int i = root(p);
        int j = root(q);
        if (size[i] < size[j]) {  // union by size
            parent[i] = j;
            size[j] += size[i];
        } else {
            parent[j] = i;
            size[i] += size[j];
        }
    }
};

typedef struct Graph {
    int** Adj;
    int numVertices;
    vector<Edge> edgeSet;
    Graph(int n) {
        numVertices = n;
        Adj = get2DMatrix(numVertices, numVertices);
    }
    void addEdge(int src, int dest, int weight) {
        Adj[src][dest] = weight;
        Adj[dest][src] = weight;
        Edge e;
        e.src = src;
        e.dest = dest;
        e.weight = weight;
        edgeSet.push_back(e);
    }
    void printGraph() {
        printf("Matrix\n");
        for (int r = 0; r < numVertices; r++) {
            for (int c = 0; c < numVertices; c++) {
                cout << Adj[r][c] << "  ";
            }
            cout << endl;
        }
        cout << endl;
    }

    vector<Vertex> getMstByPrims() {
        vector<Vertex> minHeap;
        vector<Vertex> vertexSet(numVertices);

        int r = 0;
        vertexSet[r].name = 0;
        vertexSet[r].key = 0;
        vertexSet[r].parent = -1;
        minHeap.push_back(vertexSet[r]);

        for (int i = 1; i < numVertices; i++) {
            vertexSet[i].name = i;
            vertexSet[i].key = 9999;
            vertexSet[i].parent = -1;
            minHeap.push_back(vertexSet[i]);
        }

        make_heap(minHeap.begin(), minHeap.end(), vertex_comparator());

        while (!minHeap.empty()) {
            // Extract min
            int u = minHeap.front().name;
            pop_heap(minHeap.begin(), minHeap.end(), vertex_comparator());
            minHeap.pop_back();

            for (int v = 0; v < numVertices; v++) {
                if (Adj[u][v] > 0) {
                    Vertex neighbor;
                    neighbor.name = v;
                    auto itr = find(minHeap.begin(), minHeap.end(), neighbor);
                    // v in HEAP
                    if (itr != minHeap.end() && Adj[u][v] < itr->key) {
                        vertexSet[v].parent = u;
                        vertexSet[v].key = Adj[u][v];
                        itr->parent = u;
                        itr->key = Adj[u][v];
                    }
                }
            }
            make_heap(minHeap.begin(), minHeap.end(), vertex_comparator());
        }
        return vertexSet;
    }

    vector<Edge> getMstByKruskal() {
        UnionFind uf(numVertices);
        vector<Edge> ret;
        sort(edgeSet.begin(), edgeSet.end(), edge_comparator());
        for (int i = 0; i < edgeSet.size(); i++) {
            if (uf.root(edgeSet[i].src) != uf.root(edgeSet[i].dest)) {
                ret.push_back(edgeSet[i]);
                uf.Union(edgeSet[i].src, edgeSet[i].dest);
            }
        }
        return ret;
    }
} graph;

int main() {
    graph g(9);
    // vector<vector<int>> e = {
    //     {0, 1, 10}, {0, 2, 30}, {0, 3, 40}, {1, 2, 20}, {2, 3, 50}};

    vector<vector<int>> e = {{0, 1, 4},  {0, 7, 8},  {1, 2, 8}, {1, 7, 11},
                             {2, 3, 7},  {2, 5, 4},  {2, 8, 2}, {3, 4, 9},
                             {3, 5, 14}, {4, 5, 10}, {5, 6, 2}, {6, 7, 1},
                             {6, 8, 6},  {7, 8, 7}};

    for (int i = 0; i < e.size(); i++) {
        g.addEdge(e[i][0], e[i][1], e[i][2]);
    }
    vector<Vertex> res = g.getMstByPrims();
    cout << "Mst by prims" << endl;
    for (auto x : res) {
        cout << x.name << "  " << x.parent << "  " << x.key << endl;
    }
    cout << "MST by kruskal\n";
    vector<Edge> ret = g.getMstByKruskal();
    for (auto x : ret) {
        cout << x.src << " " << x.dest << " " << x.weight << endl;
    }

    return 0;
}
// Mst by prims
// 0  -1  0
// 1  0  10
// 2  1  20
// 3  0  40
// MST by kruskal
// 0 1 10
// 1 2 20
// 0 3 40

// Mst by prims
// 0  -1  0
// 1  0  4
// 2  1  8
// 3  2  7
// 4  3  9
// 5  2  4
// 6  5  2
// 7  6  1
// 8  2  2
// MST by kruskal
// 6 7 1
// 2 8 2
// 5 6 2
// 0 1 4
// 2 5 4
// 2 3 7
// 0 7 8
// 3 4 9