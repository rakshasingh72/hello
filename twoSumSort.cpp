//Time:O(nlogn)  Space:O(n)
//Link:https://leetcode.com/problems/two-sum/

class Solution
{
  public:
    int bin_search(vector<int> &nums, int l, int h, int key)
    {
        int mid;

        while (l <= h)
        {
            mid = (l + h) / 2;
            if (key == nums[mid])
                return mid;
            else if (key < nums[mid])
                h = mid - 1;
            else
                l = mid + 1;
        }
        return -1;
    }
    vector<int> twoSum(vector<int> &nums, int target)
    {

        int size = nums.size();
        vector<int> org = nums;
        sort(nums.begin(), nums.end());
        int n1, n2;

        for (int i = 0; i < nums.size(); i++)
        {

            int x = bin_search(nums, i + 1, size - 1, target - nums[i]);
            if (x != -1)
            {
                n1 = nums[i];
                n2 = nums[x];
                break;
            }
        }

        pair<int, int> idx = {-1, -1};
        for (int j = 0; j < size; j++)
        {
            if (org[j] == n1 && idx.first == -1)
            {
                idx.first = j;
            }
            else if (org[j] == n2 && idx.second == -1)
            {
                idx.second = j;
            }
        }
        // return increasing order
        if (idx.first > idx.second) return {idx.second, idx.first};

        return {idx.first, idx.second};
    }
};

//Time:O(n)   Space:O(n)   Using Hashtable
//Link: https://leetcode.com/problems/two-sum/

class Solution
{
  public:
    vector<int> twoSum(vector<int> &nums, int target)
    {
        unordered_map<int, int> map;
        pair<int, int> idx = {-1, -1};
        int n2;
        for (int i = 0; i < nums.size(); i++)
        {
            auto itr = map.find(target - nums[i]);
            if (itr != map.end()) //found second number
            {
                idx.first = i;
                idx.second = itr->second;
            }
            map.insert({nums[i], i});
        }

        // return increasing order
        if (idx.first > idx.second) return {idx.second, idx.first};

        return {idx.first, idx.second};
    }
};