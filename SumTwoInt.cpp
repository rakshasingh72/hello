// TC: O(1) Space:O(1)
// Link: https://leetcode.com/problems/sum-of-two-integers/

class Solution {
   public:
    int getSum(int a, int b) {
        int A, B, C = 0, res = 0;
        array<int, 32> D;
        D.fill(0);
        for (int i = 0; i < 32; i++) {
            A = a & 1;
            B = b & 1;
            D[i] = A ^ B ^ C;
            C = A & B | A & C | B & C;
            a = a >> 1;
            b = b >> 1;
        }
        for (int i = 31; i >= 0; i--) {
            res = res << 1;
            res = res | D[i];
        }
        return res;
    }
};

//-2147483648