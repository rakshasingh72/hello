/* Adjacency List Graph in C */
#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

typedef struct Graph {
    int numVertices; 
    struct Node **Adj;

    Graph(int v) {
        Adj = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest);
        newNode->next = Adj[src];
        newNode->weight = w;
        Adj[src] = newNode;
    }
    struct Node *createNode(int v) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = v;
        newNode->next = NULL;
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = Adj[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
} graph;

int main() {
    int v = 4;
    vector<vector<int>> edges = {{0, 1, 1}, {0, 2, 1}, {0, 3, 1}, {1, 0, 1},
                                 {1, 2, 1}, {1, 3, 1}, {2, 0, 1}, {2, 1, 1},
                                 {2, 3, 1}, {3, 0, 1}, {3, 1, 1}, {3, 2, 1}};

    graph g(v);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }
    g.printGraph();
    return 0;
}
//  Adjacency list
// [0]: 3, 2, 1, 
// [1]: 3, 2, 0, 
// [2]: 3, 1, 0, 
// [3]: 2, 1, 0, 