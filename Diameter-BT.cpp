// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/diameter-of-binary-tree/

class Solution {
   public:
    int diameter;
    int diameterOfBinaryTree(TreeNode* root) {
        diameter = 0;
        depth(root);
        return diameter;
    }
    int depth(TreeNode* root) {
        if (root == NULL) return 0;
        int left = depth(root->left);
        int right = depth(root->right);
        diameter = max(diameter, left + right);
        return 1 + max(left, right);
    }
};
