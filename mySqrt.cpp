// TC:O(log x)  space:O(1)
// Link: https://leetcode.com/problems/sqrtx/

class Solution {
   public:
    int mySqrt(int x) {
        unsigned int m, res;
        if (x == 0) return 0;
        int l = 1, h = x;
        while (l <= h) {
            m = l + (h - l) / 2;

            if (m <= x / m) {
                res = m;
                l = m + 1;
            } else if (m > x / m) {
                h = m - 1;
            }
        }
        return res;
    }
};