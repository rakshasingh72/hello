// TC: O(n) Space: O(1)
// Link: https://leetcode.com/problems/reverse-string/

void reverseString(char* s, int n) {
    int i = 0;
    char c;
    n--;
    while (i < n) {
        c = s[i];
        s[i] = s[n];
        s[n] = c;
        i++;
        n--;
    }
}