#include <bits/stdc++.h>
using namespace std;

typedef struct Queue {
    int front;
    int rear;
    int capacity;
    int *Adj;

    void queue_init(int size) {
        Adj = (int *)malloc(sizeof(int) * size);
        if (Adj == NULL) {
            printf("error in allocating memory\n");
            return;
        }
        front = -1;
        rear = -1;
        capacity = size;
    }
    Queue() {
        queue_init(5);  // default size
    }
    Queue(int size) { queue_init(size); }
    void enqueue(int val) {
        if ((rear + 1) % capacity == front) {
            // printf("Queue full \n");
            return;
        }
        if (front == -1) {
            front++;
        }
        rear = (rear + 1) % capacity;
        Adj[rear] = val;
    }
    int dequeue() {
        if (front == -1) {
            //  printf("Queue empty \n");
            return -1;
        }
        if (front == rear) {
            front = -1;
            rear = -1;
            return 0;
        }
        front = (front + 1) % capacity;
        return 0;
    }
    int get_front() {
        if (front == -1) {
            //  printf("Queue empty \n");
            return -1;
        }
        return Adj[front];
    }
    int get_rear() {
        if (rear == -1) {
            printf("Queue empty \n");
            return -1;
        }
        return Adj[rear];
    }
    void show() {
        if (front == -1) {
            printf("Queue empty \n");
            return;
        }
        printf("Current queue:  ");
        if (rear >= front) {
            for (int i = front; i <= rear; i++) {
                printf("%d  ", Adj[i]);
            }
            printf("\n");
        } else {
            for (int i = front; i < capacity; i++) {
                printf("%d  ", Adj[i]);
            }
            for (int i = 0; i <= rear; i++) {
                printf("%d  ", Adj[i]);
            }
            printf("\n");
        }
    }
    int isEmpty() {
        if (front == -1) {
            return 1;
        }
        return 0;
    }
} queue;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};
class comparator {
   public:
    int operator()(const pair<int, int> &p1, const pair<int, int> &p2) {
        return p1.first > p2.first;
    }
};
typedef struct Graph {
    int numVertices;
    struct Node **Adj;

    Graph(int v) {
        Adj = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest, w);
        newNode->next = Adj[src];
        Adj[src] = newNode;
    }
    struct Node *createNode(int v, int w = 0) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        if (newNode != NULL) {
            newNode->vertex = v;
            newNode->weight = w;
            newNode->next = NULL;
        }
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = Adj[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }

    void dfs(int v) {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));
        // for all unvisited v call dfsHelper
        for (int i = 0; i < numVertices; i++) {
            dfsHelper(v, visited);
            v = (v + 1) % numVertices;
        }
    }
    void dfsHelper(int v, int *visited) {
        if (!visited[v]) {
            visited[v] = 1;
            cout << v << "  ";
            struct Node *neighbor = Adj[v];
            while (neighbor != NULL) {
                if (visited[neighbor->vertex] == 0) {
                    dfsHelper(neighbor->vertex, visited);
                }
                neighbor = neighbor->next;
            }
        }
    }

    void bfs(int src) {
        int visited[numVertices];
        Queue q(numVertices);
        memset(visited, 0, numVertices * sizeof(int));
        for (int i = 0; i < numVertices; i++) {
            if (visited[src] == 0) {
                bfsHelper(src, visited, &q);
            }
            src = (src + 1) % numVertices;
        }
    }
    void bfsHelper(int v, int visited[], Queue *q) {
        visited[v] = 1;
        cout << v << "  ";
        struct Node *neighbor = Adj[v];

        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                visited[neighbor->vertex] = 1;
                q->enqueue(neighbor->vertex);
            }
            neighbor = neighbor->next;
        }
        if (!(q->isEmpty())) {
            v = q->get_front();
            q->dequeue();
            bfsHelper(v, visited, q);
        }
    }

    void bfsIter(int src) {
        int visited[numVertices];
        Queue q(numVertices);
        memset(visited, 0, numVertices * sizeof(int));
        for (int i = 0; i < numVertices; i++) {
            if (visited[src] == 0) {
                q.enqueue(src);
                bfsIterHelper(src, visited, &q);
            }
            src = (src + 1) % numVertices;
        }
    }
    void bfsIterHelper(int v, int visited[], Queue *q) {
        while (!(q->isEmpty())) {
            v = q->get_front();
            q->dequeue();
            visited[v] = 1;
            cout << v << "  ";
            struct Node *neighbor = Adj[v];

            while (neighbor != NULL) {
                if (visited[neighbor->vertex] == 0) {
                    visited[neighbor->vertex] = 1;
                    q->enqueue(neighbor->vertex);
                }
                neighbor = neighbor->next;
            }
        }
    }

    vector<int> minDistanceByBfs(int src) {
        int visited[numVertices];
        Queue q(numVertices);
        // <vertex,distance from src>
        unordered_map<int, int> *mapPtr = new unordered_map<int, int>;
        memset(visited, 0, numVertices * sizeof(int));
        mapPtr->insert({src, 0});
        minDistanceHelper(src, visited, &q, *mapPtr);
        for (int i = 0; i < numVertices; i++) {
            if (visited[i] == 0) {
                mapPtr->insert({i, -1});
            }
        }
        return mapToVector(*mapPtr);
    }
    vector<int> mapToVector(unordered_map<int, int> &m) {
        vector<int> vec(numVertices);

        for (auto itr = m.begin(); itr != m.end(); itr++) {
            int vertex = itr->first;
            vec[vertex] = (itr->second);
        }
        return vec;
    }
    void minDistanceHelper(int v, int visited[], Queue *q,
                           unordered_map<int, int> &m) {
        visited[v] = 1;
        unordered_map<int, int>::iterator itr = m.find(v);
        int count = itr->second;
        struct Node *neighbor = Adj[v];
        // visiting children of v
        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                m.insert({neighbor->vertex, count + 1});
                visited[neighbor->vertex] = 1;
                q->enqueue(neighbor->vertex);
            }
            neighbor = neighbor->next;
        }
        if (!q->isEmpty()) {
            int child = q->get_front();
            q->dequeue();
            minDistanceHelper(child, visited, q, m);
        }
    }
    vector<int> runDijkstra(int src) {
        vector<int> d(numVertices);
        for (auto &x : d) {
            x = 999;
        }
        d[src] = 0;
        //<distance from src,vertex number>
        priority_queue<pair<int, int>, vector<pair<int, int>>, comparator> pq;
        for (int i = 0; i < numVertices; i++) {
            pq.push({d[i], i});
        }
        while (!pq.empty()) {
            auto u = pq.top();
            pq.pop();
            int minVertex = u.second;
            Node *neighbor = Adj[minVertex];
            while (neighbor != NULL) {
                if (d[minVertex] + neighbor->weight < d[neighbor->vertex]) {
                    d[neighbor->vertex] = d[minVertex] + neighbor->weight;
                    pq.push({d[neighbor->vertex], neighbor->vertex});
                }
                neighbor = neighbor->next;
            }
        }

        return d;
    }

    bool IsDag() {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));
        unordered_set<int> recStack;
        for (int i = 0; i < numVertices; i++) {
            if (visited[i] == 0) {
                if (!IsDagHelper(i, visited, recStack)) {
                    return false;
                }
            }
        }
        return true;
    }

    bool IsDagHelper(int v, int *visited, unordered_set<int> &recStack) {
        visited[v] = 1;
        recStack.insert(v);
        struct Node *neighbor = Adj[v];
        while (neighbor != NULL) {
            if (recStack.find(neighbor->vertex) !=
                recStack.end()) {  // cycle present
                return false;
            }
            if (visited[neighbor->vertex] == 0) {
                if (!IsDagHelper(neighbor->vertex, visited, recStack)) {
                    return false;
                };
            }
            neighbor = neighbor->next;
        }
        recStack.erase(v);
        return true;
    }

    vector<int> topologicalSort() {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));

        vector<int> ret(numVertices);
        vector<int> finishTime(numVertices, 0);
        for (int i = 0; i < numVertices; i++) {
            topologicalSortHelper(i, visited, finishTime);
        }
        // storing vertices in decreasing order of their finish times
        for (int i = 0; i < finishTime.size(); i++) {
            int max = i;
            for (int j = 0; j < finishTime.size(); j++) {
                if (finishTime[j] > finishTime[max]) {
                    max = j;
                }
            }
            ret[i] = max;
            finishTime[max] = -1;
        }
        return ret;
    }
    bool topologicalSortHelper(int v, int *visited, vector<int> &finishTime) {
        visited[v] = 1;
        static int fin = 0;
        struct Node *neighbor = Adj[v];
        while (neighbor != NULL) {
            if (visited[neighbor->vertex] == 0) {
                topologicalSortHelper(neighbor->vertex, visited, finishTime);
            }
            neighbor = neighbor->next;
        }
        if (finishTime[v] == 0) {
            finishTime[v] = ++fin;
        }
    }

} graph;

int main() {
    int num_vertices = 5;
    int src = 0;
    vector<vector<int>> edges = {
        {0, 2, 1}, {0, 1, 1}, {1, 3, 1}, {1, 4, 1}, {4, 2, 1}};

    graph g(num_vertices);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }

    g.printGraph();
    cout << "DFS order" << endl;
    g.dfs(src);
    cout << "\n BFS Order" << endl;
    g.bfs(src);

    cout << "\n BFS Iterative function order" << endl;
    g.bfsIter(src);

    vector<int> vec = g.minDistanceByBfs(src);

    cout << "\n Min distance by bfs to all vertices from src " << src << endl;
    int i = 0;
    for (auto x : vec) {
        cout << "vertex " << i++ << ":  " << x << endl;
    }

    vector<int> distance = g.runDijkstra(src);
    cout << "Min distance to all vertices from src by dijkstra " << src << endl;
    for (int i = 0; i < distance.size(); i++) {
        cout << src << "->" << i << ": " << distance[i] << endl;
    }
    cout << "Is DAG:(1 means no cycle present) " << g.IsDag() << endl;

    cout << "Topological order" << endl;
    vector<int> order = g.topologicalSort();
    for (auto x : order) {
        cout << x << "  ";
    }
    return 0;
}

//  Adjacency list
// [0]: 1, 2,
// [1]: 4, 3,
// [2]:
// [3]:
// [4]: 2,
// DFS order
// 0  1  4  2  3
//  BFS Order
// 0  1  2  4  3
//  BFS Iterative function order
// 0  1  2  4  3
//  Min distance by bfs to all vertices from src 0
// vertex 0:  0
// vertex 1:  1
// vertex 2:  1
// vertex 3:  2
// vertex 4:  2
// Min distance to all vertices from src by dijkstra 0
// 0->0: 0
// 0->1: 1
// 0->2: 1
// 0->3: 2
// 0->4: 2
// Is DAG:(1 means no cycle present) 1
// Topological order
// 0  1  3  4  2