// TC: O(n)   Space:O(n)
// Link: https://leetcode.com/problems/contains-duplicate/

class Solution {
   public:
    bool containsDuplicate(vector<int>& nums) {
        unordered_set<int> s;
        for (int i = 0; i < nums.size(); i++) {
            if (s.insert(nums[i]).second == false) {
                return true;
            }
        }
        return false;
    }
};