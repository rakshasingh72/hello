//Time:O(n)   space:O(1)
// Link: https://leetcode.com/problems/remove-duplicates-from-sorted-array/

class Solution
{
  public:
    int removeDuplicates(vector<int> &nums)
    {

        int size = nums.size();
        if (size < 2)
            return size;

        int lastCopied = nums[0], idx = 1, i = 1;

        while (i < size)
        {
            if (lastCopied != nums[i])
            {
                nums[idx] = nums[i];
                idx++;
                lastCopied = nums[i];
            }
            i++;
        }
        return idx;
    }
};