// TC: O(n)  Space: O(1)
// Link: https://leetcode.com/problems/rotate-list/

class Solution {
   public:
    ListNode* rotateRight(ListNode* head, int k) {
        ListNode* cur = head;
        int n = 0;
        while (cur) {
            cur = cur->next;
            n++;
        }

        if (n != 0) {
            k = k % n;
        }

        if (n < 2 || k == 0) {
            return head;
        }
        int x = n - k - 1;
        cur = head;
        while (x > 0) {
            cur = cur->next;
            x--;
        }
        ListNode* newHead = cur->next;
        cur->next = NULL;
        ListNode* newCur = newHead;
        while (newCur && newCur->next) {
            newCur = newCur->next;
        }
        if (newCur) newCur->next = head;

        return newHead;
    }
};