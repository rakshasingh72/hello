// TC: O(2^n)  Space: O(n*2^n) for output
// Link: https://leetcode.com/problems/letter-case-permutation/

class Solution {
   public:
    void toggleCase(string &S, int i) {
        if (islower(S[i])) {
            S[i] = toupper(S[i]);
        } else {
            S[i] = tolower(S[i]);
        }
    }
    void helper(string S, int i, vector<string> &ret) {
        if (i >= S.length()) {
            ret.push_back(S);
            return;
        }
        helper(S, i + 1, ret);
        if (isalpha(S[i])) {
            toggleCase(S, i);
            helper(S, i + 1, ret);
        }
    }
    vector<string> letterCasePermutation(string S) {
        vector<string> ret;
        helper(S, 0, ret);
        return ret;
    }
};