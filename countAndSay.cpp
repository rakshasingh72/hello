// TC:(nm)  m=length of nth term
// Link:https://leetcode.com/problems/count-and-say/

class Solution {
   public:
    string count(string res) {
        int c, j = 0;
        char t;
        string str;
        while (res[j] != '\0') {
            t = res[j];
            c = 0;
            while (t == res[j]) {
                c++;
                j++;
            }
            str = str + to_string(c) + t;
        }
        return str;
    }

    string countAndSay(int n) {
        string res = "1";
        if (n == 1) {
            return res;
        }
        for (int i = 2; i <= n; i++) {
            res = count(res);
        }
        return res;
    }
};