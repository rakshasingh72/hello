// TC:O(n)  n=size of s  space:O(n)
// Link: https://leetcode.com/problems/valid-palindrome/

bool isPalindrome(char* s) {
    bool valid = true;

    if (*s == '\0' || *(s + 1) == '\0') return valid;
    char* t = s;
    while (*t != '\0') {
        t++;
    }
    t--;
    while (s <= t) {
        if (!('a' <= *s && *s <= 'z' || 'A' <= *s && *s <= 'Z' ||
              '0' <= *s && *s <= '9')) {
            s++;
            continue;
        }
        if (!('a' <= *t && *t <= 'z' || 'A' <= *t && *t <= 'Z' ||
              '0' <= *t && *t <= '9')) {
            t--;
            continue;
        }
        if (*s >= 'A' && *s <= 'Z')  // convert to lower case
        {
            *s = 'a' + *s - 'A';
        }
        if (*t >= 'A' && *t <= 'Z')  // convert to lower case
        {
            *t = 'a' + *t - 'A';
        }
        if (*s != *t) {
            valid = false;
            break;
        }
        s++;
        t--;
    }
    return valid;
}