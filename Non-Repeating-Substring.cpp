// TC: O(2n)  Space: O(n) map
// Link:https://leetcode.com/problems/longest-substring-without-repeating-characters/

class Solution {
   public:
    int lengthOfLongestSubstring(string s) {
        if (s.length() < 2) return s.length();
        int l = 0, r = 1, gloMax = 1, curMax = 1;
        unordered_map<char, int> charMap;  // s[i],i
        charMap.insert({s[l], l});
        charMap.insert({'\0', s.length()});
        while (r <= s.length()) {
            unordered_map<char, int>::iterator itr = charMap.find(s[r]);
            if (itr != charMap.end()) {  // s[r] found
                curMax = r - l;
                gloMax = max(curMax, gloMax);
                int dup_char_idx = itr->second;
                for (int i = l; i <= dup_char_idx; i++) {
                    charMap.erase(s[i]);
                }
                l = dup_char_idx + 1;  // new charMap starting index
            }
            charMap.insert({s[r], r});
            r++;
        }
        return gloMax;
    }
};