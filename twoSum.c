/**  TC:O(n^2)  S:O(1)
  * Link:https://leetcode.com/problems/two-sum/
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int n, int target) {
    
    int * idx=malloc(2*sizeof(int));
    
    for(int i=0;i<n-1;i++)
    {       
        idx[0]=i;
        
        for(int j=i+1;j<n;j++)
        {        
            if((nums[i]+nums[j])==target)
            {
                idx[1]=j;
                return idx;
            }
        }
     }
   
    return idx;
}
