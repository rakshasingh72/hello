//TC:O(n)
//space:O(1) 


int maxSubArray(int* nums, int n) {
    
    int curMax=nums[0],gloMax=nums[0];
    
    for(int i=1;i<n;i++)
    {
        (curMax+nums[i]>nums[i])?(curMax=curMax+nums[i]):(curMax=nums[i]);
        if(curMax>gloMax)
            gloMax=curMax;
    }
    
    return gloMax;
}