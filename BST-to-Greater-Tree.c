// TC:O(n)  Space:O(height) recursion
// Link: https://leetcode.com/problems/convert-bst-to-greater-tree/

void helper(struct TreeNode* root, int* x) {
    if (!root) return;
    helper(root->right, x);
    *x = *x + root->val;
    root->val = *x;
    helper(root->left, x);
}
struct TreeNode* convertBST(struct TreeNode* root) {
    int sum = 0;
    helper(root, &sum);
    return root;
}