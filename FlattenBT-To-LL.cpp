// TC: O(n)  Space: O(height) recursion
// Link: https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
class Solution {
   public:
    void flatten(TreeNode *root) {
        TreeNode *prev = NULL;
        flatten(root, prev);
    }
    TreeNode *flatten(TreeNode *root, TreeNode *prev) {
        if (root == NULL) return prev;
        prev = flatten(root->right, prev);
        prev = flatten(root->left, prev);
        root->right = prev;
        root->left = NULL;
        return root;
    }
};