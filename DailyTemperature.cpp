// TC: O(n)  Space: O(n) Stack
// Link: https://leetcode.com/problems/daily-temperatures/

class Solution {
   public:
    vector<int> dailyTemperatures(vector<int>& T) {
        vector<int> ret(T.size());
        stack<pair<int, int>> S;  //<j,T[j]>
        for (int curDay = 0; curDay < T.size(); curDay++) {
            while (!(S.empty()) && S.top().second < T[curDay]) {
                pair<int, int> coldDay = S.top();
                S.pop();
                ret[coldDay.first] = curDay - coldDay.first;
            }
            S.push({curDay, T[curDay]});
        }
        return ret;
    }
};
