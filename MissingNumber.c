// TC: O(n)  Space:O(1)
// Link: https://leetcode.com/problems/missing-number/

int missingNumber(int* nums, int numsSize) {
    int Arr_sum = 0, i = 0, N_sum;
    while (i < numsSize) {
        Arr_sum += nums[i++];
    }
    N_sum = numsSize * (numsSize + 1) / 2;
    return (N_sum - Arr_sum);
}