// TC: O(n)  Space:O(n) set
// Link: https://leetcode.com/problems/two-sum-iv-input-is-a-bst/

class Solution {
   public:
    bool findTarget(TreeNode* root, int k) {
        unordered_set<int> set;
        return f(root, k, set);
    }
    bool f(TreeNode* root, int k, unordered_set<int>& set) {
        if (root) {
            auto itr = set.find(k - root->val);
            if (itr != set.end()) {
                return true;
            }
            set.insert(root->val);
            return f(root->left, k, set) || f(root->right, k, set);
        }
        return false;
    }
};