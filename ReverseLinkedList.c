// TC: O(n)  Space: O(n)
// Link: https://leetcode.com/problems/reverse-linked-list/

struct ListNode* h = NULL;
void rev(struct ListNode* prev, struct ListNode* cur);

struct ListNode* reverseList(struct ListNode* head) {
    struct ListNode* prev = NULL;
    struct ListNode* cur = head;
    h = head;
    if (cur == NULL) {
        printf("NULL");
        return h;
    }
    if (cur->next == NULL) {
        printf("%d->NULL", cur->val);
        return h;
    }
    rev(prev, cur);
    return h;
}
void rev(struct ListNode* prev, struct ListNode* cur) {
    if (!cur) {
        h = prev;
        return;
    }
    rev(cur, cur->next);
    cur->next = prev;
}