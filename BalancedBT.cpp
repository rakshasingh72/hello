// TC: O(n^2) Top down Approach  Space:O(height) recursion
// Link: https://leetcode.com/problems/balanced-binary-tree/

class Solution {
   public:
    int height(struct TreeNode* root) {
        if (root == NULL) return 0;

        return 1 + max(height(root->left), height(root->right));
    }

    bool isBalanced(struct TreeNode* root) {
        if (root == NULL) return true;

        return abs((height(root->left)) - (height(root->right))) <= 1 &&
               isBalanced(root->left) && isBalanced(root->right);
    }
};