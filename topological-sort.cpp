#include <bits/stdc++.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

struct Node {
    int vertex;
    int weight;
    struct Node *next;
};

typedef struct Graph {
    int numVertices; 
    struct Node **A;

    Graph(int v) {
        A = (struct Node **)calloc(v, sizeof(struct Node *));
        numVertices = v;
    }
    void addEdge(int src, int dest, int w) {
        struct Node *newNode = createNode(dest);
        newNode->next = A[src];
        newNode->weight = w;
        A[src] = newNode;
    }
    struct Node *createNode(int v) {
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
        newNode->vertex = v;
        newNode->next = NULL;
        return newNode;
    }
    void printGraph() {
        printf("\n Adjacency list\n");
        for (int v = 0; v < numVertices; v++) {
            struct Node *temp = A[v];

            printf("[%d]: ", v);
            while (temp != NULL) {
                printf("%d, ", temp->vertex);
                temp = temp->next;
            }
            printf("\n");
        }
    }
  vector<int> topologicalSort() {
        int visited[numVertices];
        memset(visited, 0, numVertices * sizeof(int));

        vector<int> ret(numVertices);
        vector<int> finishTime(numVertices, 0);
        for (int i = 0; i < numVertices; i++) {
                dfsHelper(i, visited, finishTime);
        }
        //storing vertices in decreasing order of their finish times 
        for (int i = 0; i < finishTime.size(); i++) {
            int max = i;
            for (int j = 0; j < finishTime.size();j++) {
                if(finishTime[j]>finishTime[max]){
                    max = j;
                }
            }
            ret[i] = max;
            finishTime[max] = -1;
        }
        return ret;
  }
  bool dfsHelper(int v, int *visited, vector<int> &finishTime) {
      visited[v] = 1;
      static int fin = 0;
      struct Node *neighbor = A[v];
      while (neighbor != NULL) {
          if (visited[neighbor->vertex] == 0) {
              dfsHelper(neighbor->vertex, visited, finishTime);
          }
          neighbor = neighbor->next;
      }
      if(finishTime[v]==0){
          finishTime[v] = ++fin;
      }
      
    }
} graph;

int main() {
   int v = 6;
    vector<vector<int>> edges = {{2, 3, 1}, {3, 1, 1}, {4, 0, 1}, 
                                 {4, 1, 1}, {5,2,1} ,{5,0,1} };

    graph g(v);
    for (int i = 0; i < edges.size(); i++) {
        g.addEdge(edges[i][0], edges[i][1], edges[i][2]);
    }

    g.printGraph();
    cout << "Topological order" << endl;
    vector<int> order = g.topologicalSort();
    for(auto x:order){
        cout << x << "  ";
    }
    return 0;
}
//  Adjacency list
// [0]: 
// [1]: 
// [2]: 3, 
// [3]: 1, 
// [4]: 1, 0, 
// [5]: 0, 2, 
// Topological order
// 5  4  2  3  1  0 