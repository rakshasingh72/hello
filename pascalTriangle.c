// TC: O(n^3) n=no. of rows   Space: O(n)
// Link: https://leetcode.com/problems/pascals-triangle/

/**
 * Return an array of arrays.
 * The sizes of the arrays are returned as *columnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume
 * caller calls free().
 */

int comb(int n, int r) {
    long long int res = 1;
    for (int i = 1; i <= r; i++) {
        res = (res * n) / i;
        n = n - 1;
    }
    return res;
}

int** generate(int numRows, int** columnSizes) {
    int** gen = malloc(numRows * sizeof(int*));

    int* col = malloc(numRows * sizeof(int));

    *columnSizes = col;

    for (int i = 0; i < numRows; i++) {
        int* arr = malloc((i + 1) * sizeof(int));
        for (int j = 0; j < i + 1; j++) {
            arr[j] = comb(i, j);
        }
        gen[i] = arr;
        col[i] = i + 1;
    }

    return gen;
}