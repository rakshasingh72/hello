#include <iostream>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

using namespace std;

template <class T>
void printVector(vector<T> v)
{
    cout << "Vector elements: ";
    for (int i = 0; i < v.size(); i++)
        cout << v[i] << "  ";
    cout << endl;
}
void vectorDemo(vector<int> v)
{
    cout << "v size:" << v.size() << endl;
    cout << "pushed 100" << endl;
    v.push_back(100);
    cout << "first element=" << v.front() << endl
         << "last element=" << v.back() << endl;
    cout << "pop_back" << endl;
    v.pop_back();
    cout << "Last element=" << v.back() << endl;
}
void vectorConcat(vector<int> &v1, vector<int> &v2)
{

    v1.insert(v1.end(), v2.begin(), v2.end());
}
void vectorSum(vector<int> v)
{
    int sum = 0;
    sum = accumulate(v.begin(), v.end(), sum);
    cout << "Vector sum=" << sum << endl;
}
int compare(int x, int y)
{
    return x > y;
}
int compareByLen(string s1, string s2)
{
    return s1.size() < s2.size();
}
void vectorQuestion()
{
    //1.Create a vector with 10 random integer and increment each element by 1.

    vector<int> v1 = {10, 11, 12, 13, 14, 15, 6, 7, 8, 9};

    printVector(v1);

    int &x = v1.front();

    for (int i = 0; i < v1.size(); i++)
        v1[i]++;

    cout << "After incrementing each element by 1" << endl;
    printVector(v1);

    //2.Concatenate two vectors of ints.
    vector<int> v2 = {40, 50};

    vectorConcat(v1, v2);
    cout << "After concatenating v1,v2 " << endl;
    printVector(v1);

    //3.Sum all elements.
    vectorSum(v1);

    /*
4.Create a vector with 10 random integer and use STL functions only to perform the following 
  operations on the vector. 
 a) find the max and min elements. 
 b) Reverse this vector. 
 c) Erase integer 3rd element from last 
 d) sort this vector in increasing order. 
 e) Test if a number (say 15) is present in this vector using Binary search algorithm
  (hint: user lower_bound / upper_bound) 
 f) insert 2 elements (4 and 10) at 3rd position from last. 
 g) d) sort this vector in decreasing order. 
 h) Test if 17 is present. i) swap first and last integer
 */
    vector<int> v = {14, 2, 13, 5, 66, 7};
    printVector(v);

    vector<int>::iterator it1 = max_element(v.begin(), v.end());
    vector<int>::iterator it2 = min_element(v.begin(), v.end());
    cout << "Max=" << *it1 << "  Min" << *it2 << endl;

    sort(v.begin(), v.end());
    cout << "After sorting" << endl;
    printVector(v);

    cout << "After Reverse 0 to 2 idx" << endl;
    reverse(v.begin(), v.begin() + 3);
    printVector(v);

    cout << "Erasing 3rd element from last" << endl;
    v.erase(v.end() - 3);
    printVector(v);

    sort(v.begin(), v.end());
    bool keyFound = binary_search(v.begin(), v.end(), 14);
    cout << "key 14 found=" << keyFound << endl;

    cout << "Adding 4,10 at position 3" << endl;
    vector<int>::iterator it = v.insert(v.begin() + 3, 4);
    v.insert(it, 10);
    printVector(v);

    sort(v.begin(), v.end(), compare);
    cout << "After sorting" << endl;
    printVector(v);

    cout << "Swapping first and last element" << endl;
    //iter_swap(v.begin(), v.end() - 1);
    swap(v.front(), v.back());
    printVector(v);

    //5.How to get a direct pointer to the memory array used internally by vector?
    int *ptr = v.data();
    cout << "*(++ptr)" << *(++ptr) << endl;

    //6.Create a vector of 10 random strings and use STL functions only to perform
    // sorting by string length
    vector<string> str = {"raksha", "tony", "poony"};
    sort(str.begin(), str.end(), compareByLen);
    cout << "String sort\n";
    for (string i : str)
    {
        cout << i << "  " << endl;
    }
}
void printSet(unordered_set<int> us)
{
    cout << "Unordered_set: ";
    for (auto itr = us.begin(); itr != us.end(); itr++)
    {
        cout << *itr << "  ";
    }
    cout << "\n set size=" << us.size() << endl;
}
void unorderedSet()
{
    // 1. Create an unordred set of 10 random ints.
    cout << endl;

    unordered_set<int> intSet;

    srand(time(0));

    for (int i = 0; i < 10; i++)
    {
        intSet.insert(rand() % 100);
    }
    printSet(intSet);

    //2.Insert an element, delete an element, Test the presence of an element.
    //Find max and min elements.
    unordered_set<int> s1 = {2, 4, 5, 6, 7};
    s1.insert(100);
    s1.erase(5);
    printSet(s1);
    unordered_set<int>::iterator keyPtr = s1.find(100);

    if (keyPtr == s1.end())
        cout << "Not found\n";
    else
        cout << "Found " << *keyPtr << endl;

    cout << "Max element= " << *max_element(s1.begin(), s1.end()) << endl;
    cout << "Min element= " << *min_element(s1.begin(), s1.end()) << endl;

    //3.What underlying data structure is used?
    //What is the time complexity of insert, delete, check, max, and min operations?
    //DS=Hashtable
    //TC:O(1) for insert,delete,check
    //TC:O(n) for min,max
}
int comp(string i, string j)
{
    return i < j;
}
void unorderedMap()
{
    cout << endl;
    //1.Create an unordered map of 10 random elements, where keys are strings and values as ints.
    unordered_map<string, int> umap;
    umap["how"] = 4;
    umap["to"] = 12;
    umap["code"] = 0;
    umap["fast"] = 3;
    //2.Insert a key and value, update the value of a key, delete a key.
    umap.insert({"?", 1});
    auto itr = umap.find("code");
    if (itr != umap.end())
    {
        itr->second = 2;
    }
    umap.erase("fast");
    cout << "Unordered map:\n";
    for (auto x : umap)
    {
        cout << "key=" << x.first << "  value=" << x.second << endl;
    }
    //3.Find max and min key

    string maxKey = umap.begin()->first;
    string minKey = umap.begin()->first;
    for (auto itr = umap.begin(); itr != umap.end(); itr++)
    {
        if (itr->first > maxKey)
            maxKey = itr->first;

        if (itr->first < maxKey)
            minKey = itr->first;
    }
    cout << "Maxkey in unordered map=" << maxKey << endl;
    cout << "Minkey =" << minKey << endl;

    //4.What underlying data structure is used?
    //What is the time complexity of insert, find, delete, max, min operations?
    //DS: Hashtable    TC: O(1) for find,insert,delete,  O(n) for min,max
}
void heap()
{
    //1. Create a max heap of 10 random ints.
    vector<int> v = {3, 3, 5, 7, 8, 5, 2, 44};

    make_heap(v.begin(), v.end());
    cout << "\n Max in heap=" << v.front() << endl;

    for (int x : v)
    {
        cout << x << "  ";
    }
    cout << endl;
    //2.Insert an element, delete max element.
    v.push_back(99);
    push_heap(v.begin(), v.end());
    v.push_back(101);
    push_heap(v.begin(), v.end());

    pop_heap(v.begin(), v.end());
    v.pop_back();

    cout << "Heap elements:";
    for (int x : v)
        cout << x << "  ";
    cout << endl;

    //3.What underlying data structure is used to store the elements?
    //What is the complexity of create (heap), insert, delete max, get max operations?
    //DS:Vector
    //TC: make_heap:O(n)  push_heap:O(log n)  pop_heap:O(logn)  max:O(1)
}
void setFunc()
{
    //1.Create a set of 10 random ints.
    int arr[] = {21, 32, 43, 54, 65};
    set<int> s1(arr, arr + 5);
    //2.Insert an element, delete an element, Test the presence of an element.
    // Find max and min elements.
    s1.insert(14);
    s1.erase(43);
    auto itr = s1.find(21);

    cout << "\nSet elements:";
    for (auto x : s1)
        cout << x << " ";
    cout << endl;

    if (itr == s1.end())
        cout << "Not found\n";
    else
        cout << "Found=" << *itr << endl;

    cout << "Set Max=" << *max_element(s1.begin(), s1.end()) << "  "
         << "Set Min=" << *min_element(s1.begin(), s1.end()) << endl;

    //3.What underlying data structure is used?
    // What is the time complexity of insert, find, delete, max, min operations?
    //DS for Set:Balanced BST  TC:O(logn):insert,find,delete; O(1):max, min
}
void mapFunc()
{
    cout << endl;
    //1.Create a map of 10 random elements, where keys are strings and values as ints
    map<string, int> m = {{"how", 1}, {"to", 2}, {"think", 3}, {"logically", 0}};

    //2.Insert a key and value, update the value of a key, delete a key.
    m.insert({"z", 4});
    m.insert(pair<string, int>("hi", 5));

    m.erase("logically");

    auto itr = m.find("Logic");
    if (itr == m.end())
        cout << "Key \"Logic\" not found in Map\n";
    else
        cout << "Key found in Map" << itr->first << endl;

    //3.Find max and min key
    auto itrStart = m.begin();
    cout << "Min key in Map=" << itrStart->first << endl;

    auto itrEnd = m.rbegin();
    cout << "Max key in Map=" << itrEnd->first << endl;

    cout << "Map key   value\n";
    for (auto x : m)
    {
        cout << "    " << x.first << "     " << x.second << endl;
    }
    //4.What underlying data structure is used?
    //What is the time complexity of insert, find, delete, max, min operations?
    //DS for Map:Balanced BST   TC:O(logn):insert,find,delete; O(1):min,max
}
int main()
{
    vectorQuestion();
    unorderedSet();
    unorderedMap();
    heap();
    setFunc();
    mapFunc();
}