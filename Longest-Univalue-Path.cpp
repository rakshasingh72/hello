// TC: O(n^2)  Space:O(n)
// Link: https://leetcode.com/problems/longest-univalue-path/

class Solution {
   public:
    int curLongest = 0;
    int longestUnivaluePath(TreeNode* root) {
        max(curLongest, longest(root));
        if (root) {
            // curLongest = max(curLongest, longestUnivaluePath(root->left));
            max(curLongest, longestUnivaluePath(root->left));
            max(curLongest, longestUnivaluePath(root->right));
        }
        return curLongest;
    }

    // gives the longest univalue path passing through root
    int longest(TreeNode* root) {
        if (root == NULL) return 0;
        int lval = 0, rval = 0, len = 0;
        if (root->left != NULL && root->val == root->left->val) {
            lval = 1 + longest(root->left);
        }
        if (root->right != NULL && root->val == root->right->val) {
            rval = 1 + longest(root->right);
        }
        len = lval + rval;
        curLongest = max(curLongest, len);

        return max(lval, rval);
    }
};